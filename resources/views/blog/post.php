<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="keywords" content="zabava,vtipy,obrazky,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <meta property="og:title" content="<?=$post->title?>"/>
    <meta property="og:description" content="Zábavné a vtipné obrázky. Klikni pre zobrazenie!"/>
    <meta property="og:site_name" content="Coolovo.eu - Zábavné a vtipné obrázky"/>

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="//fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="/dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="/dist/css/blog.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="/dist/img/favicon.png" type="image/png" />
    <link rel="manifest" href="/manifest.json">

    <title><?=$post->title?> - Blog Coolovo.eu</title>
<body>
    <?php include(__DIR__ . '/../parts/navbar.php'); ?>

    <?php include(__DIR__ . '/../parts/noscript.php'); ?>

    <div id="spinner" class="spinner"></div>
    <div id="scrollable-content" style="padding-top: 0; display: none">
        <div id="content">
            <div id="whitespace" style="height: 1em;"></div>

            <!-- Link to all posts. -->
            <a class="blog-btn" href="/blog"><i class="fa fa-arrow-left"></i> späť na zoznam</a>

            <!-- Actual post. -->
            <h2 class="blog-title"><?=$post->title?></h2>
            <small>Dátum: <?=$post->date?> | Autor: <?=$post->author?></small>
            <div class="blog-content"><?=$post->content?></div>

            <h2>Komentáre</h2>
            <div class="comments-section">
                <div id="disqus_thread"></div>
                <script type="text/javascript">
                    /* * * CONFIGURATION VARIABLES * * */
                    var disqus_shortname = 'coolovo';

                    /* * * DON'T EDIT BELOW THIS LINE * * */
                    (function(e) { if(!e) return;
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })(false);
                </script>
            </div>
            <div id="whitespace" style="height: 4em;"></div>
        </div>
        <footer>
            <small>
                <a href="/latest">latest</a> | <a href="/about">about</a> | <a href="/blog">blog</a> | <a href="/developers">api</a>
            </small>
            <br/>
            <small>&copy; Coolovo.eu 2015 | Version 1.0</small>
        </footer>
        <div id="whitespace" style="height: 4em;"></div>
    </div>

    <div id="scrollToTop">
        <i class="fa fa-chevron-up"></i>
    </div>

    <div id="overlay">
        <div id="settings">
            <h2>Settings</h2>
            <p>Settings are saved automatically.</p>
            <div id="settings_holder">

            </div>
            <span id="settings-close-btn" class="btn ns">Close</span>
        </div>
    </div>

    <script>dontBoot = true;</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
    <!-- We should inline this js, because it's small. -->
    <!-- <script src="/dist/js/chromefix.js"></script> -->
    <script id="chromefix-js">window.addEventListener("load",function(){var t=0,e=!1,n=function(n){1==n.touches.length&&(t=n.touches[0].clientY,e=0==window.pageYOffset)},o=function(n){var o=n.touches[0].clientY,d=o-t;return t=o,e&&(e=!1,d>0)?void n.preventDefault():void 0};document.addEventListener("touchstart",n,!1),document.addEventListener("touchmove",o,!1)});</script>
    <script src="/dist/js/touch.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-56027601-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="/dist/js/app.js"></script>
</body>
</html>