<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="description" content="Obrázok #<?=$image_id?>. Meme obrázky a ostatné zábavné obrázky z českého a slovenského internetu ako na dlani. Denne pribudne približne 30 nových obrázkov!">
    <meta name="keywords" content="zabava,vtipy,obrazky,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <meta property="og:title" content="#<?=$image_id?>"/>
    <meta property="og:description" content="Zábavné a vtipné obrázky. Klikni pre zobrazenie!"/>
    <meta property="og:site_name" content="Coolovo.eu - Zábavné a vtipné obrázky"/>
    <meta property="og:image" content="https://<?=$_SERVER['SERVER_NAME']?>/api/image/<?=$image_id?>"/>
    <meta property="og:url" content="https://<?=$_SERVER['SERVER_NAME']?>/<?=$image_id?>"/>

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="//fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="dist/img/favicon.png" type="image/png" />
    <link rel="manifest" href="/manifest.json">

    <title>Zábavné a vtipné obrázky - #<?=$image_id?> - Coolovo.eu</title>
<body>
    <?php include(__DIR__ . '/parts/navbar.php'); ?>

    <?php include(__DIR__ . '/parts/noscript.php'); ?>

    <div id="spinner" class="spinner"></div>
    <div id="scrollable-content" style="padding-top: 0; display: none">

        <!-- If user hasn't accepted EU cookie shit, show message. -->
        <?php include(__DIR__ . '/parts/cookies.php'); ?>

        <h2>Zdieľané s vami:</h2>
        <div id="shared" class="shared">
            <div class="shared-image" style="background-image: url('api/image/<?=$image_id?>')"></div>
        </div>
        <h2>Posledné obrázky:</h2>
        <div id="gallery" style="-webkit-filter:grayscale(1)" onmouseover="$(this).css('-webkit-filter','grayscale(0)');this.onmouseover = null;">
            <?=$galleryHtml?>
        </div>
    </div>

    <div id="scrollToTop">
        <i class="fa fa-chevron-up"></i>
    </div>

    <?php include(__DIR__ . '/parts/settings.php'); ?>

    <?php include(__DIR__ . '/parts/viewer.php'); ?>

    <script>dontBoot = false;</script>
    <script>sharedImage = '<?=$image_id?>';</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
    <!-- We should inline this js, because it's small. -->
    <!-- <script src="/dist/js/chromefix.js"></script> -->
    <script id="chromefix-js">window.addEventListener("load",function(){var t=0,e=!1,n=function(n){1==n.touches.length&&(t=n.touches[0].clientY,e=0==window.pageYOffset)},o=function(n){var o=n.touches[0].clientY,d=o-t;return t=o,e&&(e=!1,d>0)?void n.preventDefault():void 0};document.addEventListener("touchstart",n,!1),document.addEventListener("touchmove",o,!1)});</script>
    <script src="/dist/js/touch.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-56027601-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="/dist/js/app.js"></script>

    <?php include( __DIR__ . '/parts/rollbar.php') ;?>
</body>
</html>