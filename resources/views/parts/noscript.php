<noscript>
    <p>Coolovo.eu je stránka, ktorá zhromažduje rôzne zábavné a vtipné obrázky z internetu a predovšetkým Facebooku a následne ich zobrazuje na tejto stránke.
        Umožnuje tak používateľom rýchly, dostupný a ľahký spôsob, ako takéto zábavné meme a iné obrázky sledovať.</p>
    <p>Na zvoju funkciu ale potrebuje povolený Javascript. Bez neho nemôže fungovať!</p>
</noscript>