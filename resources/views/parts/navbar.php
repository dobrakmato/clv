<div id="navbar" class="ns">
    <h1 style="display: none">Zábavné a vtipné obrázky - <?=$category?> - Coolovo.eu</h1>
    <!-- <a class="logo" href="/">Coolovo.eu</a> -->
    <a title="Posledné obrázky" href="/latest" class="first"><i class="fa fa-clock-o"></i><span style="display: none;">posledné obrázky</span></a>
    <a title="Náhodné obrázky" href="/random"><i class="fa fa-cube"></i><span style="display: none;">náhodné obrázky</span></a>
    <a title="Najlepšie obrázky" class="disabled" href="/best"><i class="fa fa-star"></i><span style="display: none;">najlepšie obrázky</span></a>
    <a title="Obľúbené obrázky" class="disabled" href="/liked"><i class="fa fa-heart"></i><span style="display: none;">obľúbené obrázky</span></a>
    <a title="O projekte" href="/about" class="last"><i class="fa fa-question-circle"></i><span style="display: none;">o projekte</span></a>
    <i id="settings-btn" rel="nofollow,nocache" title="Nastavenia a experimenty" class="fa fa-cog dd"></i>
</div>