<div id="viewer">
    <div class="viewer-image" style="background-image: url('data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')">
        <div id="close-btn" class="close ns"><i class="fa fa-times"></i></div>
        <div class="arrow-prev"><i class="fa fa-chevron-left"></i></div>
        <div class="arrow-next"><i class="fa fa-chevron-right"></i></div>
    </div>
    <div class="viewer-info">
        <div class="inner">
            <h2 class="viewer-title"></h2>

            <div class="clearfix">
                <span id="like-btn" class="btn ns likes disabled" title="Dať like"><i class="fa fa-thumbs-up"></i> <span class="like-count">0</span></span>
                <span id="dislike-btn" class="btn ns dislikes disabled" title="Dať dislike"><i class="fa fa-thumbs-down"></i> <span class="dislike-count">0</span></span>
                <span id="download-btn" class="btn ns download" title="Stiahnuť obrázok"><i class="fa fa-download"></i></span>
                <span id="report-btn" class="btn ns report" title="Nahlásiť obrázok"><i class="fa fa-exclamation-triangle"></i></span>
                <span id="fshare-btn" class="btn ns fshare" title="Zdielať na facebooku"><i class="fa fa-facebook-official"></i></span>
                <span id="twittershare-btn" class="btn ns twittershare" title="Zdielať na twitteri"><i class="fa fa-twitter"></i></span>
            </div>
            <div style="padding-top: 0.5em;color:#888;font-size: 0.8em">
                <p>Originál: <i class="fa fa-external-link"></i> <a target="_blank" href="" id="image-original"></a><br/>
                    Nahrané: <span id="image-date">unknown</span>
                </p>

                <p>Textový prepis (experimentálne): <span style="color:#aaa" id="image-transcript"></span></p>
            </div>
        </div>
    </div>
</div>
