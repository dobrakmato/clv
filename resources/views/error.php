<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

    <meta name="description" content="Meme obrázky a ostatné zábavné obrázky z českého a slovenského internetu ako na dlani. Denne pribudne približne 30 nových obrázkov!">
    <meta name="keywords" content="zabava,vtipy,obrazky,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="//fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="/dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="/dist/img/favicon.png" type="image/png" />
    <link rel="manifest" href="/manifest.json">

    <title>Zábavné a vtipné obrázky - Chyba! - Coolovo.eu</title>
</head>
<body>

    <div style="margin: 0 auto; max-width: 800px; padding: 1em;color: #fff">
        <div id="content">
            <div style="text-align: center;max-width: 100%;"><img src="//i.imgur.com/uMNumUd.jpg" style="max-width: 100%" title="darth vader noooo"></div>
            <h2>Chyba!</h2>
                <p>Na strane servera nastala neočakávaná chyba! O tejto chybe pravdepodobne vieme a čoskoro začneme pracovať na jej odstránení.</p>
            <h2>Čo teraz?</h2>
            <?php

            use App\Libraries\ImageRepo;

            if(isset($e) && $e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException): ?>
                <?php
                $iid = str_replace('/', '', $_SERVER["REQUEST_URI"]);

                $id = image_id($iid);

                // Find matching pictures.
                $images = ImageRepo::getLike($iid);

                ?>
                <p>Možno ste hľadali jednu z týchto stránok (zoradené od najnovšieho po najstaršie):</p>

                <?php if(count($images) == 15): ?>
                    <p style="color: #aaa;">Kvôli veľkému množstvu výsledkov sa zobrazuje iba prvých 15.</p>
                <?php endif; ?>

                <ul>
                    <?php if(count($images) == 0): ?>
                        <li style="font-style: italic">nenašli sa žiadne podobajúce sa stránky</li>
                    <?php endif; ?>

                    <?php foreach($images as $file): ?>
                    <li><a href="/<?=$file?>" style="color: #6fbeff"><?=base56_encode($file)?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <p>Môžete ísť na <a href="/" style="color:#fff;text-decoration: underline;">hlavnú stránku</a>.</p>
        </div>
    </div>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-56027601-3', 'auto');
        ga('send', 'pageview');
    </script>
    <?php include( __DIR__ . '/parts/rollbar.php') ;?>
</body>
</html>