<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="description" content="Obrázok #<?= $image_id ?>. Meme obrázky a ostatné zábavné obrázky z českého a slovenského internetu ako na dlani. Denne pribudne približne 30 nových obrázkov!">
    <meta name="keywords" content="zabava,vtipy,obrazky,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <?php if (isset($_GET["style"]) && $_GET["style"] == "dark"): ?>
        <link href="/dist/css/embed_dark.css" rel="stylesheet" type="text/css">
    <?php else: ?>
        <link href="/dist/css/embed.css" rel="stylesheet" type="text/css">
    <?php endif; ?>
    <link rel="icon" href="/dist/img/favicon.png" type="image/png"/>

    <title>Zábavné a vtipné obrázky - #<?= $image_id ?> - Coolovo.eu</title>
</head>
<body>
    <div class="content">
        <div class="image" style="background-image: url('<?= $url ?>')">
            <a href="https://www.coolovo.eu"><img class="logo" src="https://www.coolovo.eu/dist/img/coolovo_embed.png"></a>
        </div>
    </div>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-56027601-3', 'auto');
        ga('send', 'pageview');
    </script>
    <?php include( __DIR__ . '/parts/rollbar.php') ;?>
</body>
</html>