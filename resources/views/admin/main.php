<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="description" content="Meme obrázky a ostatné zábavné obrázky z českého a slovenského internetu ako na dlani. Aktualizované viac krát za deň!">
    <meta name="keywords" content="obrazky,zabava,vtipy,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="/dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="/dist/css/admin.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="/dist/img/favicon.png" type="image/png"/>
    <link rel="manifest" href="/manifest.json">

    <title>Administrácia Coolovo.eu</title>
</head>
<body>
    <?php include('parts/_navbar.php');?>

    <div id="main">
        <div id="sidebar-parent">
            <?php include('parts/_sidebar.php');?>
        </div>
        <div id="content-parent">
            <div id="content">
                <!-- Include content. -->
                <?php include('parts/'. $part . '.php'); ?>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
</body>
</html>