<div id="inner-content">
    <h1>Cache</h1>

    <h2>Information</h2>

    <p>
        File: <b><?= $file ?></b><br/>
        Size: <b><?= round($size / 1024, 2) ?> kb</b><br/>
        Cached images: <b><?= $lines ?></b><br/>
        Cache lifetime: <b><?= $lifetime ?> seconds (<?= round($lifetime / 60, 1) ?> minutes)</b><br/>
        Last regenerated: <b><?= date("d.M.Y H:i:s", $lastModified) ?></b><br/>
        Next regeneration (expected): <b><?= date("d.M.Y H:i:s", $lastModified + $lifetime) ?></b><br/>
    </p>

    <h2>Actions</h2>

    <p>
        <span class="btn ns" title="Regenerate cache"><i class="fa fa-refresh"></i> Regenerate cache</span>
        <span class="btn ns" title="Clear cache"><i class="fa fa-trash-o"></i> Clear cache</span>
    </p>
</div>