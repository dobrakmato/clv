<div id="inner-content">
    <h1>Stats</h1>
    <?php
    $json = json_decode(file_get_contents("https://www.coolovo.eu/api/stats?full"));
    ?>

    <h2>Počet obrázkov za deň</h2>
    <div id="graph" style="height: 7em; width: 1100px;">
        <?php foreach($json->full as $day): ?>
            <div data-count="<?=count($day)?>" style="display: inline-block;bottom: 0px;margin-left: 1px; height: <?=count($day) / $json->maxPerDay * 100?>%; background: linear-gradient(to bottom, #81CBFF 0%, #0A72BB 100%); width: <?=100/($json->totalDays * 1.1)?>%"></div>
        <?php endforeach; ?>
    </div>
</div>