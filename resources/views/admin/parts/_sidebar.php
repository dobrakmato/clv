<div id="sidebar">
    <ul>
        <li><a href="/admin/dashboard"><i class="fa fa-tachometer"></i> dashboard</a></li>
        <li><a href="/admin/cache"><i class="fa fa-space-shuttle"></i> cache</a></li>
        <li><a href="/admin/images"><i class="fa fa-picture-o"></i> images</a></li>
        <li><a href="/admin/log"><i class="fa fa-file-text-o"></i> log</a></li>
        <li><a href="/admin/reports"><i class="fa fa-exclamation-triangle"></i> reports</a></li>
        <li><a href="/admin/stats"><i class="fa fa-bar-chart"></i> stats</a></li>
        <li><a id="sidebar-fade-out"></a></li>
    </ul>
</div>