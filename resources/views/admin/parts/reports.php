<div id="inner-content">
    <h1>Reports</h1>

    <?php foreach($reports as $report): ?>
        <?php $parts = explode("|", $report); ?>
        <div class="report">
            <div class="report-image" style="background-image: url('//www.coolovo.eu/api/image/<?=$parts[2]?>')"></div>
            <div class="report-text">
                <p>Date: <?=date("d.M.Y H:i:s", strtotime($parts[1]))?></p>
                <p>IP: <?=$parts[0]?></p>
                <p>Message: <?=$parts[3]?></p>
                <!-- Action buttons -->
                <div class="actions">
                    <span class="btn ns" title="Open image"><i class="fa fa-picture-o"></i> Open image</span>
                    <span class="btn ns" title="Resolve report"><i class="fa fa-check"></i> Resolve</span>
                    <span class="btn ns" title="Remove report"><i class="fa fa-trash-o"></i> Remove</span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>