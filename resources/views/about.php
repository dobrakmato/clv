<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <meta name="description" content="Meme obrázky a ostatné zábavné obrázky z českého a slovenského internetu ako na dlani. Aktualizované viac krát za deň!">
    <meta name="keywords" content="obrazky,zabava,vtipy,meme,komix,emefka,cierny humor,pemik,sranda,internet">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="//fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="/dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="/dist/img/favicon.png" type="image/png" />
    <link rel="manifest" href="/manifest.json">

    <title>Zábavné a vtipné obrázky - čo to je? - Coolovo.eu</title>
</head>
<body>
    <?php include(__DIR__ . '/parts/navbar.php'); ?>

    <?php include(__DIR__ . '/parts/noscript.php'); ?>

    <div id="spinner" class="spinner"></div>

    <!-- This is actually content. -->
    <div id="gallery">
        <!-- If user hasn't accepted EU cookie shit, show message. -->
        <?php include(__DIR__ . '/parts/cookies.php'); ?>

        <div id="content">
            <div id="whitespace" style="height: 1em;"></div>
            <div style="text-align: center">
                <img id="coolovo-logo" src="//coolovo.eu/dist/img/coolovo_about.png" style="max-width: 100%; height: 16em;" title="coolovo.eu logo">
            </div>

            <h2 id="what-is-coolovo">Čo to je?</h2>
                <p>Coolovo.eu je <abbr title="skupina obrázkov z iných zdrojov">databáza</abbr> vtipných a zábavných obrázkov z českého a slovenského internetu, ktorá ich prezentuje na jednoduchej a prehľadnej webstránke.</p>
                <p>Je poháňaná robotmi, ktoré tieto obrázky zbierajú. Je polo-modernovaná, tzn. nezmyselný a nevtipný obsah je odstránený, ale napríklad obsah <abbr title="Not Safe For Work">NSFW</abbr> (nevhodný do práce) odstraňovaný nie je.</p>
            <h2 id="why-does-coolovo-exists">Prečo to je?</h2>
                <p>Projekt Coolovo.eu vznikol z viacerých dôvodov:
                    <ul>
                        <li>facebook stránok, ktoré repostujú obsah bolo už dosť, chce to niečo nové a lepšie</li>
                        <li>neprehľadnosť existujúcich zdrojov</li>
                        <li>malý dosah uživateľov v prípade facebooku (obyčajný užívateľ zmešká časť obsahu, kvôli spôsobu, akým facebook príspevky zobrazuje a nezobrazuje)</li>
                        <li>neexistencia inej používateľsky a obsahovo zameranej platformy (platforma, ktorá sa zaoberá <strong>iba vitpnými obrázkami</strong> a prezentuje ich užívateľom jednoducho a prístupne)</li>
                    </ul>
                </p>
            <h2 id="this-is-not-your-content">Kopíruješ!</h2>
                <p>Máš pravdu, ale nikto netvrdil, že tu je vlastný obsah. Je to predsa databáza obrázkov. Pointa stránky je <b>zhromaždiť existujúci obsah na jedno miesto</b>. <b>Autori stránky nie sú autormi obrázkov!</b></p>
                <p>Okrem toho veľa (ak nie všetci) autori obrázkov do nich pridávajú vodoznak alebo odkaz, ktorý vedie naspäť ku ním. Naša stránka tento vodoznak nemaže, práve naopak, <b>podporujeme pôvodných autorov obrázkov</b>!</p>
                <p>Ak si aj napriek tomu myslíš, že je niečo v neporiadku, môže obrázok nahlásiť pomocou tlačidla s trojuhloníkom - "nahlásiť obrázok".</p>
            <h2 id="add-to-home-screen">Pridať na domácu obrazovku</h2>
                <p>Ak túto webstránku používaš často, možno by bolo pre teba dobré, ak by si si ju pridal na domovskú obrazovku. Pýtaš sa prečo? Obrázok ti povie viac: </p>
                <div style="text-align: center">
                    <div style="width: 40%; display: inline-block; padding: 1em"><img style="width: 100%" src="//i.imgur.com/l7yJ6nq.jpg" title="verzia v prehliadači"></div>
                    <div style="width: 40%; display: inline-block; padding: 1em"><img style="width: 100%" src="//i.imgur.com/CPpkTG9.jpg" title="verzia ako odkaz na domovskej orbazovke"></div>
                </div>
                <p>Ako na to? Keď práve prezeráš Coolovo.eu na mobile, tak klikni v prehliadači na 3 bodky (v pravo hore), vyber možnosť <code>Pridať na domovskú obrazovku</code> (alebo <code>Add to Home screen</code> v angličtine), potvrď okno a si hotový!</p>
            <h2 id="want-contribute">Našiel som chybu alebo mám nápad</h2>
                <p>Potom si super človek (samozrejme iba ak sa o to s nami podelíš)! Môžes nás zatiaľ kontaktovať iba email-om na <code>dobrakmato@gmail.com</code>.</p>
                <p>A ak si programátor, ktorý má skúsenosti s PHP (OOP), Laravelom, MySQL, SASS, TypeScriptom, image processingom alebo neuronovými sieťami a chcel by si nejako pomôcť, tiež neváhaj a kontaktuj nás!</p>
            <h2 id="statistics">Štatistika</h2>
                <script>
                    function loadStats() {
                        $.getJSON('https://www.coolovo.eu/api/stats', function(data) {
                            var html = "";
                            html += "Dní: <b>" + data.totalDays + "</b><br/>";
                            html += "Obrázkov: <b>" + data.totalImages + "</b><br/>";
                            html += "Priemerne za deň: <b>~" + Math.floor(data.avgPerDay) + "</b><br/>";
                            html += "Frekvencia: <b>každých ~" + Math.floor(24 / data.avgPerDay * 60) + " minút</b><br/>";
                            html += "Minimálne za deň: <b>" + data.minPerDay + "</b><br/>";
                            html += "Maximálne za deň: <b>" + data.maxPerDay + "</b><br/>";
                            html += "Veľkosť: <b>~" + Math.floor(data.sizeOnDisk / 1024 / 1024) + " MB</b><br/>";
                            $("#stats").html(html);
                        });
                    }
                </script>
                <p id="stats"></p>
                <p><a href="#statistics" style="color: #8CC1E6" onclick="$(this).parent().hide();loadStats();">Načítať štatistiku.</a></p>

            <h2 id="credits">Poďakovania</h2>
            <p>Matej Kormuth, matej.kormuth@gmail.com</p>
            <p>Samuel Fukumori, fukumori.samuel@gmail.com</p>

            <?php include(__DIR__ . '/parts/footer.php'); ?>

            <p id="whitespace" style="height: 2em"></p>
        </div>
    </div>

    <div id="scrollToTop">
        <i class="fa fa-chevron-up"></i>
    </div>

    <?php include(__DIR__ . '/parts/settings.php'); ?>

    <!-- Make gallery script don't execute. -->
    <script>dontBoot = true;</script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
    <!-- We should inline this js, because it's small. -->
    <!-- <script src="/dist/js/chromefix.js"></script> -->
    <script id="chromefix-js">window.addEventListener("load",function(){var t=0,e=!1,n=function(n){1==n.touches.length&&(t=n.touches[0].clientY,e=0==window.pageYOffset)},o=function(n){var o=n.touches[0].clientY,d=o-t;return t=o,e&&(e=!1,d>0)?void n.preventDefault():void 0};document.addEventListener("touchstart",n,!1),document.addEventListener("touchmove",o,!1)});</script>
    <script src="/dist/js/touch.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-56027601-3', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
        $("#coolovo-logo").click(function () {
            // Blur them
            $("#gallery").velocity({"blur": 5}, 300);
            $("#navbar").velocity({"blur": 5}, 300);
            // After 400 ms show the logo.
            var holder = $("<div/>")
                .attr('id','created-holder')
                .css('position', 'fixed')
                .css('top', '0')
                .css('bottom', '0')
                .css('left', '0')
                .css('right', '0');
            holder.appendTo(document.body);

            var logo = $("<img/>")
                .attr('id', 'created-logo')
                .attr('src', 'https://coolovo.eu/dist/img/coolovo_logo.png')
                .css('margin', '10em auto')
                .css('max-width', '90%')
                .css('opacity', '0.0');
            logo.appendTo(holder);
            logo.velocity('transition.bounceIn', 1000);

            function cleanUp() {
                $("#created-logo").velocity('transition.expandOut', 400, function() {
                    $(this).remove();
                    $("#created-holder").remove();
                });
                // Revert blur
                $("#gallery").velocity({"blur": 0}, 300);
                $("#navbar").velocity({"blur": 0}, 300);
            }

            logo.click(cleanUp.bind(null));
            holder.click(cleanUp.bind(null));
        });
    </script>
    <script src="/dist/js/app.js"></script>
    <?php include( __DIR__ . '/parts/rollbar.php') ;?>
</body>
</html>