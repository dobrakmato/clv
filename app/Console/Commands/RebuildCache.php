<?php

namespace App\Console\Commands;

use App\Libraries\ImageRepo;
use Illuminate\Console\Command;

class RebuildCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imgcache:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild image cache file';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Rebuilding image cache...');
        // Rebuild image cache.
        ImageRepo::rebuildCache();
        $this->info('Image cache file rebuilt!');
    }
}