<?php

namespace App\Libraries;

/**
 * Represents image repository on disk drive. Thi class also generates image
 * cache file which is used to speed up queries by caching amount of images
 * on disk and names of this images.
 *
 * @package App\Libraries
 */
class ImageRepo
{
    /**
     * Height of thumbnail.
     */
    const TARGET_HEIGHT = 256;
    /**
     * Quality of thumbnail.
     */
    const TARGET_QUALITY = 50;


    static $fileExtension;
    static $searchPath;
    static $cacheFile;
    static $cacheContents = null;
    static $cacheLifetime = 300; // 5 minutes

    /**
     * Static class initialization in Java fashion.
     */
    public static function clinit()
    {
        if (self::$cacheContents == null) {
            self::$cacheFile = storage_path("cache.txt");
        }
    }

    /**
     * Returns array of found files in repository folder.
     * @return array array of images
     */
    public static function search()
    {
        return glob(self::$searchPath . '/*.' . self::$fileExtension);
    }

    /**
     * Returns array of files from repository with specified start position and count ordered chronologically.
     *
     * @param $start int start position, must be greater then 0
     * @param $count int amount of images, must be smaller then 100
     * @param bool|false $override whether to override count limit (allow more then 100 images in single call)
     * @return array array of files by specified arguments
     */
    public static function get($start, $count, $override = false)
    {
        // Preconditions.
        if ($start < 0) {
            throw new \InvalidArgumentException("Start must be value bigger or equal to zero.");
        }
        if ($count < 0) {
            throw new \InvalidArgumentException("Count must be value bigger or equal to zero.");
        }
        if ($count > 100 && !$override) {
            throw new \InvalidArgumentException("Count must be value smaller then 100.");
        }

        $files = array();
        $cache = new \SplFileObject(self::$cacheFile);

        // Get total lines in cache (First line is image count). Don't forget to trim the \n from
        // the images counts.
        $totalLines = str_replace("\r", '', str_replace("\n", '', $cache->current()));

        if ($start > $totalLines) {
            throw new \OutOfRangeException("Start position ($start) is too damn high ($totalLines)!");
        }

        // Check for overflow and fix count.
        if ($totalLines < $start + $count) {
            $count = $totalLines - $start;
        }

        // Seek to specified amount of images.
        $cache->seek($start + 1);
        // Then output count images.
        for ($i = 0; $i < $count; $i++) {
            $files[] = $cache->current();
            $cache->next();
        }

        // Dispose this file handler.
        unset($cache);

        return $files;
    }

    /**
     * Returns array of files from repository with specified start position and count ordered by shuffling
     * with specified seed.
     *
     * @param $start int start position, must be greater then 0
     * @param $count int amount of images, must be smaller then 100
     * @param $seed int seed of shuffle
     * @param bool|false $override whether to override count limit (allow more then 100 images in single call)
     * @return array array of files by specified arguments
     */
    public static function getBySeed($start, $count, $seed, $override = false)
    {
        if ($start < 0) {
            throw new \InvalidArgumentException("Start must be value bigger or equal to zero.");
        }
        if ($count < 0) {
            throw new \InvalidArgumentException("Count must be value bigger or equal to zero.");
        }
        if ($count > 100 && !$override) {
            throw new \InvalidArgumentException("Count must be value smaller then 100.");
        }

        $end = $start + $count;

        // Read all lines.
        $lines = file(self::$cacheFile);
        // Actual amount of images.
        $imageAmount = count($lines) - 1;
        $files = array();

        $startIndex = $seed % $imageAmount;

        for ($i = $start; $i < $end; $i++) {
            $index = ($startIndex + ($i * 13 + $i * $seed)) % $imageAmount;
            $files[] = $lines[$index + 1];
        }

        return $files;
    }

    /**
     * Rebuilds image list cache.
     */
    public static function rebuildCache()
    {
        $files = self::search();

        info("Rebuilding cache from " . self::$searchPath . " with extension " . self::$fileExtension);

        // Sort them by modified date.
        usort($files, function ($a, $b) {
            return filemtime($a) < filemtime($b);
        });

        $lines = count($files);

        // Build cache. First line is amount of entries. Then each line is one entry.
        $contents = $lines . PHP_EOL;
        foreach ($files as $file) {
            $contents .= self::id($file) . PHP_EOL;
        }

        // Save cache contents.
        file_put_contents(self::$cacheFile, $contents);
    }

    /**
     * Returns whether image with specified id exists in repository.
     *
     * @param $imageId int image id
     * @return bool true if specified image exists, false otherwise
     */
    public static function exists($imageId)
    {
        return file_exists(self::file($imageId));
    }

    /**
     * Converts file path to image id.
     *
     * @param $file string file name
     * @return string id of this image
     */
    public static function id($file)
    {
        return trim(str_replace('.' . self::$fileExtension, '', str_replace(self::$searchPath . "/", '', $file)));
    }

    /**
     * Returns date of last modification of specified image.
     *
     * @param $imageId int image id
     * @return int timestamp of file addition
     */
    public static function date($imageId)
    {
        $file = self::file($imageId);
        if (file_exists($file)) {
            $res = filemtime($file);
            if ($res === false) {
                return 1;
            } else {
                return $res;
            }
        } else {
            return time() + 60;
        }
    }

    /**
     * Converts specified image if to file path.
     *
     * @param $id int id of image
     * @return string path to image file
     */
    public static function file($id)
    {
        return self::$searchPath . "/$id." . self::$fileExtension;
    }

    /**
     * Reads specified image to output stream.
     *
     * @param $id int image id
     * @return int etc
     */
    public static function read($id)
    {
        return readfile(self::file($id));
    }

    /**
     * Reads thumbnail of specified image to output stream. Also generates
     * thumbnail if necessary.
     *
     * @param $imageId int image id
     * @return int etc
     */
    public static function readThumbnail($imageId)
    {
        $path = storage_path("thumbnails/$imageId.jpg");
        if (!file_exists($path)) {
            // This should also cache thumbnails.
            $type = exif_imagetype(self::file($imageId));
            $img = null;
            if ($type == IMAGETYPE_JPEG) {
                $img = imagecreatefromjpeg(self::file($imageId));
            } else if ($type == IMAGETYPE_PNG) {
                $img = imagecreatefrompng(self::file($imageId));
            }
            list($w, $h) = getimagesize(self::file($imageId));
            $ar = $w / $h;
            $thumb = imagecreatetruecolor(self::TARGET_HEIGHT * $ar, self::TARGET_HEIGHT);
            imagecopyresampled($thumb, $img, 0, 0, 0, 0, self::TARGET_HEIGHT * $ar, self::TARGET_HEIGHT, imagesx($img), imagesy($img));
            imagejpeg($thumb, $path, self::TARGET_QUALITY);
        }
        return readfile($path);
    }

    /**
     * Returns array of file with similar ID as specified one.
     *
     * @param $imageId int image id to search like
     * @return array files with similar ids
     */
    public static function getLike($imageId)
    {
        $files = array();
        $cache = new \SplFileObject(self::$cacheFile);

        // Seek to specified amount of images.
        $cache->seek(1);
        // Then output count images.
        for ($i = 0; !$cache->eof(); $i++) {
            if (count($files) >= 15) {
                break;
            }

            if (starts_with($cache->fgets(), $imageId)) {
                $files[] = $cache->current();
            }
        }

        // Dispose this file handler.
        unset($cache);

        return $files;
    }

    /**
     * Returns date of last modification of specified image thumbnail.
     *
     * @param $imageId int image id
     * @return int timestamp of file addition
     */
    public static function thumbnailDate($imageId)
    {
        $file = storage_path("thumbnails/$imageId.jpg");
        if (file_exists($file)) {
            $res = filemtime($file);
            if ($res === false) {
                return 1;
            } else {
                return $res;
            }
        } else {
            return time() + 60;
        }
    }
}

// Static initialization.
ImageRepo::clinit();

// Load static values into class from environment.
ImageRepo::$searchPath = env_var('IR_ROOT', './');
ImageRepo::$fileExtension = env_var('IR_EXTENSION', 'jpg');
ImageRepo::$cacheLifetime = env_var('IR_CACHE_LIFETIME', 300);