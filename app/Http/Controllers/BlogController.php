<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Provides blog functionality to page.
 *
 * @package App\Http\Controllers
 */
class BlogController extends BaseController
{
    /**
     * Displays list of blog entries.
     *
     * @return \Illuminate\View\View view
     */
    function index()
    {
        // Find all blog posts.
        $path = storage_path("blog/");
        $files = glob($path . "*.json");

        $posts = [];

        // Sort them.
        usort($files, function ($a, $b) {
            return filemtime($a) < filemtime($b);
        });

        // Render excerpts from markdown.
        foreach ($files as $file) {
            $metacontents = file_get_contents($file);
            $post = json_decode($metacontents);
            $post->slug = basename($file, ".json");

            $contents = file_get_contents(storage_path("blog/" . $post->slug . ".md"));
            $contents = firstSentence($contents);
            $html = Slimdown::render($contents);
            $post->content = $html;

            $posts[] = $post;
        }

        // Render view.
        return view('blog.index', ['posts' => $posts, 'category' => 'blog']);
    }

    /**
     * Displays specified on blog entry.
     *
     * @param string $slug slug of blog entry
     * @return \Illuminate\View\View|void view
     */
    function post($slug)
    {
        $path = storage_path("blog/$slug.json");

        if (!file_exists($path)) {
            return abort(404);
        }

        // Load information about post.
        $metacontents = file_get_contents($path);
        $post = json_decode($metacontents);

        // Render markdown to HTML.
        $contents = file_get_contents(storage_path("blog/$slug.md"));
        $html = Slimdown::render($contents);
        $post->content = $html;

        // Render view.
        return view('blog.post', ['post' => $post, 'category' => 'blog']);
    }
}

/**
 * Slimdown - A very basic regex-based Markdown parser. Supports the
 * following elements (and can be extended via Slimdown::add_rule()):
 *
 * - Headers
 * - Links
 * - Bold
 * - Emphasis
 * - Deletions
 * - Quotes
 * - Inline code
 * - Blockquotes
 * - Ordered/unordered lists
 * - Horizontal rules
 *
 * Author: Johnny Broadway <johnny@johnnybroadway.com>
 * Website: https://gist.github.com/jbroadway/2836900
 * License: MIT
 */
class Slimdown
{
    public static $rules = array(
        '/(#+)(.*)/' => 'self::header',                           // headers
        '/\[([^\[]+)\]\(([^\)]+)\)/' => '<a href=\'\2\'>\1</a>',  // links
        '/(\*\*|__)(.*?)\1/' => '<strong>\2</strong>',            // bold
        '/(\*|_)(.*?)\1/' => '<em>\2</em>',                       // emphasis
        '/\~\~(.*?)\~\~/' => '<del>\1</del>',                     // del
        '/\:\"(.*?)\"\:/' => '<q>\1</q>',                         // quote
        '/`(.*?)`/' => '<code>\1</code>',                         // inline code
        '/\n\*(.*)/' => 'self::ul_list',                          // ul lists
        '/\n\-(.\-)/' => 'self::ul_list',                          // ul lists (with -)
        '/\n[0-9]+\.(.*)/' => 'self::ol_list',                    // ol lists
        '/\n(&gt;|\>)(.*)/' => 'self::blockquote ',               // blockquotes
        '/\n-{5,}/' => "\n<hr />",                                // horizontal rule
        '/\n([^\n]+)\n/' => 'self::para',                         // add paragraphs
        '/<\/ul>\s?<ul>/' => '',                                  // fix extra ul
        '/<\/ol>\s?<ol>/' => '',                                  // fix extra ol
        '/<\/blockquote><blockquote>/' => "\n"                    // fix extra blockquote
    );

    private static function para($regs)
    {
        $line = $regs[1];
        $trimmed = trim($line);
        if (preg_match('/^<\/?(ul|ol|li|h|p|bl)/', $trimmed)) {
            return "\n" . $line . "\n";
        }
        return sprintf("\n<p>%s</p>\n", $trimmed);
    }

    private static function ul_list($regs)
    {
        $item = $regs[1];
        return sprintf("\n<ul>\n\t<li>%s</li>\n</ul>", trim($item));
    }

    private static function ol_list($regs)
    {
        $item = $regs[1];
        return sprintf("\n<ol>\n\t<li>%s</li>\n</ol>", trim($item));
    }

    private static function blockquote($regs)
    {
        $item = $regs[2];
        return sprintf("\n<blockquote>%s</blockquote>", trim($item));
    }

    private static function header($regs)
    {
        list ($tmp, $chars, $header) = $regs;
        $level = strlen($chars);
        // Level 1 is reserved for main title.
        return sprintf('<h%d>%s</h%d>', $level + 1, trim($header), $level);
    }

    /**
     * Add a rule.
     */
    public static function add_rule($regex, $replacement)
    {
        self::$rules[$regex] = $replacement;
    }

    /**
     * Render some Markdown into HTML.
     */
    public static function render($text)
    {
        $text = "\n" . $text . "\n";
        foreach (self::$rules as $regex => $replacement) {
            if (is_callable($replacement)) {
                $text = preg_replace_callback($regex, $replacement, $text);
            } else {
                $text = preg_replace($regex, $replacement, $text);
            }
        }
        return trim($text);
    }
}