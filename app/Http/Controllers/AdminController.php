<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Provides administration.
 *
 * @package App\Http\Controllers
 */
class AdminController extends BaseController
{
    /**
     * Displays cache information.
     *
     * @return \Illuminate\View\View view
     */
    function cache()
    {
        $file = storage_path("cache.txt");
        $cache = new \SplFileObject($file);

        // Get total lines in cache (First line is image count). Don't forget to trim the \n from
        // the images counts.
        $totalLines = str_replace("\r", '', str_replace("\n", '', $cache->current()));

        unset($cache);
        $lastModified = filemtime($file);
        $size = filesize($file);
        $lifetime = env('IR_CACHE_LIFETIME');

        return view('admin.main', ['part' => 'cache', 'file' => $file, 'lines' => $totalLines, 'size' => $size,
            'lastModified' => $lastModified, 'lifetime' => $lifetime]);
    }

    /**
     * Displays administration dashboard.
     *
     * @return \Illuminate\View\View view
     */
    function dashboard()
    {
        return view('admin.main', ['part' => 'dashboard']);
    }

    /**
     * Displays images information.
     *
     * @return \Illuminate\View\View view
     */
    function images()
    {
        return view('admin.main', ['part' => 'images']);
    }

    /**
     * Displays logs information.
     *
     * @return \Illuminate\View\View view
     */
    function log()
    {
        return view('admin.main', ['part' => 'log']);
    }

    /**
     * Displays reports information.
     *
     * @return \Illuminate\View\View view
     */
    function reports()
    {
        $reports = array_reverse(file(storage_path("reports.txt"), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));

        return view('admin.main', ['part' => 'reports', 'reports' => $reports]);
    }

    /**
     * Displays statistical information.
     *
     * @return \Illuminate\View\View view
     */
    function stats()
    {
        return view('admin.main', ['part' => 'stats']);
    }
}