<?php

namespace App\Http\Controllers\Api;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Used to retrieve current user's authentication token using AJAX.
 * @package App\Http\Controllers
 */
class TokenController extends BaseController
{

    /**
     * Returns current user token.
     *
     * @param Request $request
     * @return string json response
     */
    public function current(Request $request)
    {
        if (true || $request->session()->has("token")) {
            $token = $request->session()->get("token");
            $validTo = 0;
            return json(array("token" => $token, "valid_to" => $validTo));
        } else {
            return json(array("error" => 8, "message" => "Permission denied!"));
        }
    }
}