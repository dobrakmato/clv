<?php

namespace App\Http\Controllers\Api;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Provides statistics endpoint to API.
 *
 * @package App\Http\Controllers
 */
class StatsController extends BaseController
{
    /**
     * Returns statistics in json form.
     *
     * @return string json response
     */
    public function viewStats()
    {
        // TODO: Refactor this garbage.

        $startTime = microtime(true);
        date_default_timezone_set('Europe/Bratislava');
        $files = glob('/usr/share/nginx/html/fbrepost/*.jpg');
        $data = array();

        usort($files, function ($a, $b) {
            return filemtime($a) < filemtime($b);
        });

        $diskSize = 0;

        foreach ($files as $file) {
            $time = getdate(filemtime($file));
            $diskSize += filesize($file);
            $data[$time['mday'] . '-' . $time['mon'] . '-' . $time['year']][] = $file;
        }

        $days = 0;
        $totalCount = 0;
        $min = 999999999;
        $max = -999999999;
        foreach ($data as $oneDay) {
            $days++;
            $count = count($oneDay);
            $totalCount += $count;
            $min = min($min, $count);
            $max = max($max, $count);
        }

        // Count also size of previews.
        $previewSize = 0;
        $files2 = glob('/usr/share/nginx/html/domains/coolovo.eu/storage/thumbnails/    *.jpg');
        foreach ($files2 as $file2) {
            $previewSize += filesize($file2);
        }

        $data2 = array();

        $data2["totalDays"] = $days;
        $data2["totalImages"] = $totalCount;
        $data2["avgPerDay"] = $totalCount / $days;
        $data2["minPerDay"] = $min;
        $data2["maxPerDay"] = $max;
        $data2["sizeOnDisk"] = $diskSize + $previewSize;

        $data2["executionTime"] = round((microtime(true) - $startTime) * 1000, 2);

        if (isset($_GET["full"])) {
            $data2["full"] = $data;
        }

        header('Content-Type: application/json');
        return json_encode($data2, JSON_PRETTY_PRINT);
    }
}