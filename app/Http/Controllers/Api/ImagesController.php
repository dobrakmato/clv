<?php

namespace App\Http\Controllers\Api;

use App\Libraries\ImageRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Provides image endpoints to API.
 *
 * @package App\Http\Controllers
 */
class ImagesController extends BaseController
{

    /**
     * Returns image (Content-Type: image/*) if exists, else returns 404.
     *
     * @param Request $request request
     * @param int|string $id id of requested image
     * @return string image or json error
     */
    public function image(Request $request, $id)
    {
        // Bypass protection for search engines.
        if (!$request->session()->has("token") &&
            !str_contains($request->headers->get('User-Agent'), "Googlebot") &&
            !str_contains($request->headers->get('User-Agent'), "bot") &&
            !str_contains(mb_strtolower($request->headers->get('User-Agent')), "facebook")
        ) {
            return json(array("error" => 8, "message" => "Permission denied!"));
        }

        // Decode ID.
        $id = image_id($id);

        // Check if image exists.
        if (!ImageRepo::exists($id)) {
            header("HTTP/1.0 404 Not Found");
            return json(array("error" => 2, "message" => "Specified image does not exists!"));
        }

        // Get size of requested image.
        $size = isset($_GET["size"]) ? $_GET["size"] : IMAGE_SIZE_LARGE;

        // If user requested thumbnail.
        if ($size == IMAGE_SIZE_THUMBNAIL) {
            return $this->returnThumbnail($id);
        } else {
            return $this->returnLarge($id);

        }
    }

    /**
     * Increments view counter for specified image by one.
     *
     * @param number|string $id id of image
     * @return string response
     */
    public function view($id)
    {
        // Decode ID.
        $id = image_id($id);

        // Check if image exists.
        if (!ImageRepo::exists($id)) {
            header("HTTP/1.0 404 Not Found");
            return json(array("error" => 2, "message" => "Specified image does not exists!"));
        }

        $file = storage_path("views.json");

        // Load views file.
        $views = json_decode(file_get_contents($file));
        // Increment view count.
        if (isset($views->$id)) {
            $views->$id = $views->$id + 1;
        } else {
            $views->$id = 1;
        }
        // Save views file.
        file_put_contents($file, json_encode($views));
    }

    /**
     * Returns information (likes, dislikes, comments) about image specified by id.
     *
     * @param int|string $id id of image
     * @return string
     */
    public function details($id)
    {
        // Decode ID.
        $id = image_id($id);

        if (!ImageRepo::exists($id)) {
            header("HTTP/1.0 404 Not Found");
            return json(array("error" => 2, "message" => "Specified image does not exists!"));
        }

        // Perform look on db.
        // $data = DB::select("SELECT COUNT(`like`) as cnt_likes, COUNT(`dislike`) as cnt_dislikes FROM `likes` WHERE `photo_id` = ?", $id);

        // Try to find transcription.
        $transcript = "";
        if (file_exists("/usr/share/nginx/html/fbrepost/index/$id.txt")) {
            $transcript = file_get_contents("/usr/share/nginx/html/fbrepost/index/$id.txt");
        }

        // Find view count.
        $file = storage_path("views.json");

        // Load views file.
        $views = json_decode(file_get_contents($file));

        $info = array(
            "date" => ImageRepo::date($id),
            "likes" => 0, //$data->cnt_likes,
            "dislikes" => 0, //$data->cnt_dislikes,
            "views" => isset($views->$id) ? $views->$id : 0,
            "transcript" => $transcript,
            "comments" => []
        );
        return json($info);
    }

    /**
     * If logged in, adds like to specified image.
     *
     * @param Request $request request
     * @param int|string $id id of image
     * @return string JSON response
     */
    public function like(Request $request, $id)
    {
        // Decode ID.
        $id = image_id($id);

        if ($request->session()->get('user_id', null) == null) {
            return json(array("error" => 4, "message" => "You must be logged in!"));
        }
        $userId = $request->session()->get('user_id', null);

        // If row for current user does not exists.
        $data = DB::select("SELECT * FROM `likes` WHERE `photo_id` = ? AND `user_id` = ?", $id, $userId);

        $hasVoted = $data->like == 1;

        if (!$hasVoted) {
            // Create a row with like.


            // Count likes and dislikes.
            $data = DB::select("SELECT COUNT(`like`) as cnt_likes, COUNT(`dislike`) as cnt_dislikes FROM `likes` WHERE `photo_id` = ?", $id);
            return json(array("error" => 0, "message" => "Image liked.", "data" => array(
                "likes" => $data->cnt_likes,
                "dislikes" => $data->cnt_dislikes
            )));
        } else {
            return json(array("error" => 1, "message" => "Image already liked."));
        }
    }

    /**
     * If logged in, adds dislike to specified image.
     *
     * @param Request $request request
     * @param int|string $id id of image
     * @return string JSON response
     */
    public function dislike(Request $request, $id)
    {
        // Decode ID.
        $id = image_id($id);

        if ($request->session()->get('user_id', null) == null) {
            return json(array("error" => 4, "message" => "You must be logged in!"));
        }
        $userId = $request->session()->get('user_id', null);

        // If row for current user does not exists.
        $data = DB::select("SELECT * FROM `likes` WHERE `photo_id` = ? AND `user_id` = ?", $id, $userId);

        $hasVoted = $data->dislike == 1;

        if ($hasVoted) {
            // Create a row with dislike.

            // Count likes and dislikes.
            $data = DB::select("SELECT COUNT(`like`) as cnt_likes, COUNT(`dislike`) as cnt_dislikes FROM `likes` WHERE `photo_id` = ?", $id);
            return json(array("error" => 0, "message" => "Image disliked.", "data" => array(
                "likes" => $data->cnt_likes,
                "dislikes" => $data->cnt_dislikes
            )));
        } else {
            return json(array("error" => 1, "message" => "Image already disliked."));
        }
    }

    /**
     * Returns thumbnail version of specified image.
     *
     * @param int|string $id image id
     * @param $CACHING_ENABLED
     * @return int image
     */
    public function returnThumbnail($id, $CACHING_ENABLED = true)
    {
        if ($CACHING_ENABLED) {
            $headers = getallheaders();
        }

        // Perform great caching!
        if ($CACHING_ENABLED && isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == ImageRepo::thumbnailDate($id))) {
            // Client's cache IS current, so we just respond '304 Not Modified'.
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', ImageRepo::thumbnailDate($id)) . ' GMT', true, 304);
            exit;
        } else {
            // Image not cached or cache outdated, we respond '200 OK' and output the image.
            header('Content-Type: image/jpeg');
            header('Cache-Control: max-age=2592000; public');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', ImageRepo::thumbnailDate($id)) . ' GMT', true, 200);

            return ImageRepo::readThumbnail($id);
        }
    }

    /**
     * Returns large version of specified image.
     *
     * @param int|string $id image id
     * @param bool $CACHING_ENABLED
     * @return int image
     */
    public function returnLarge($id, $CACHING_ENABLED = true)
    {
        // Images should be cached by browser.
        if (isset($_GET["download"])) {
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . basename($id . ".jpg") . "\"");

            return ImageRepo::read($id);
        } else {
            if ($CACHING_ENABLED) {
                $headers = getallheaders();
            }

            // Perform great caching!
            if ($CACHING_ENABLED && isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == ImageRepo::date($id))) {
                // Client's cache IS current, so we just respond '304 Not Modified'.
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', ImageRepo::date($id)) . ' GMT', true, 304);
                exit;
            } else {
                header('Content-Type: image/jpeg');
                header('Cache-Control: max-age=2592000; public');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', ImageRepo::date($id)) . ' GMT', true, 200);

                return ImageRepo::read($id);
            }
        }
    }
}
