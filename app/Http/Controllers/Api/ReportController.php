<?php

namespace App\Http\Controllers\Api;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Provides report endpoint to API.
 *
 * @package App\Http\Controllers
 */
class ReportController extends BaseController
{
    /**
     * Reports
     * @param Request $request
     * @return string
     */
    public function report(Request $request)
    {
        // Protect this feature by allowing only one report per 30 seconds.
        if (!$request->session()->has("lastReport")) {
            $request->session()->set("lastReport", 1);
        }
        if ($request->session()->get("lastReport", time()) + 30 < time()) {
            $request->session()->set("lastReport", time());

            $imageId = $_GET["id"];
            $msg = $_GET["msg"];

            // Decode ID.
            $imageId = image_id($imageId);

            if ($msg == null || $msg == "null" || empty($msg)) {
                return json(array("status" => "Message cannot be empty!"));
            }

            $msg = str_replace("|", "(sep)", $msg);

            if (!file_exists(storage_path("reports.txt"))) {
                file_put_contents(storage_path("reports.txt"), "");
            }

            $ip = $_SERVER['REMOTE_ADDR'];
            $date = date(\DateTime::RFC822, time());

            // Save to reports file.
            $reports = file_get_contents(storage_path("reports.txt"));
            $reports .= "$ip|$date|$imageId|$msg" . PHP_EOL;
            file_put_contents(storage_path("reports.txt"), $reports);

            return json(array("status" => "Thanks for your report!"));
        }
        return json(array("status" => "You can send next report after 30 seconds!"));
    }
}