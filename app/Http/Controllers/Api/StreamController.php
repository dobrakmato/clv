<?php

namespace App\Http\Controllers\Api;

use App\Libraries\ImageRepo;
use Exception;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Used to provide stream endpoints to API.
 *
 * @package App\Http\Controllers
 */
class StreamController extends BaseController
{

    /**
     * Returns JSON of best images in short period of time (one day).
     */
    public function best()
    {
        // select * from liked where date > now() - 3 * 24 * 60 * 60 order by count(like) desc
    }

    /**
     * Returns JSON of user liked images.
     *
     * @param Request $request
     * @return string
     */
    public function liked(Request $request)
    {
        if ($request->session()->get('user_id', null) == null) {
            return json(array("error" => 4, "message" => "You must be logged in!"));
        }
        $userId = $request->session()->get('user_id', null);

        // select * from likes where user_id = ? and like = 1
    }

    /**
     * Returns JSON of images specified by GET start, end values.
     *
     * @return string json
     */
    public function latest()
    {
        $start = isset($_GET["start"]) ? $_GET["start"] : 0;
        $end = isset($_GET["end"]) ? $_GET["end"] : 30;
        $count = $end - $start;

        // Find files.
        try {
            $files = ImageRepo::get($start, $count);
        } catch (\Exception $e) {
            return json(array("error" => 7, "message" => get_class($e) . ": " . $e->getMessage()));
        }

        // Limit them.
        $images = array();
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $images[] = array(
                "id" => $id,
                "date" => ImageRepo::date($id)
            );
        }

        return json($images);
    }

    /**
     * Returns stream of images by using seed.
     *
     * @param $seed string seed
     * @return string json response
     */
    public function random($seed)
    {
        $start = isset($_GET["start"]) ? $_GET["start"] : 0;
        $end = isset($_GET["end"]) ? $_GET["end"] : 30;
        $count = $end - $start;

        // Find files.
        try {
            $files = ImageRepo::getBySeed($start, $count, base56_decode($seed));
        } catch (Exception $e) {
            return json(array("error" => 7, "message" => get_class($e) . ": " . $e->getMessage()));
        }

        // Limit them.
        $images = array();
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $images[] = array(
                "id" => $id,
                "date" => ImageRepo::date($id)
            );
        }

        return json($images);
    }

    /**
     * Experimental search stream endpoint.
     *
     * @return string json response
     */
    public function search()
    {
        // Get search terms.
        $terms = isset($_GET["q"]) ? $_GET["q"] : "#default#";

        if ($terms == "#default#") {
            return json(array("error" => 11, "message" => "No search terms found in request."));
        }

        // Open fulltext cache file.
        $text = file_get_contents("/usr/share/nginx/html/fbrepost/index/big.cache");

        $lines = explode("\n", $text);

        // Create array of similarity to all results.
        $similarityArray = array();

        foreach ($lines as $line) {
            $thisLineTermsArr = explode("|", $line);
            if (count($thisLineTermsArr) < 2) {
                continue;
            }
            $thisLineTerms = $thisLineTermsArr[1];
            $similarity = 0;
            similar_text($terms, $thisLineTerms, $similarity);
            $similarityArray[$similarity] = $line;
        }

        // Sort results by best match.
        uksort($similarityArray, function ($a, $b) {
            return $a > $b;
        });

        $start = isset($_GET["start"]) ? $_GET["start"] : 0;
        $end = isset($_GET["end"]) ? $_GET["end"] : 30;
        $count = $end - $start;

        $allLines = count($similarityArray);

        if ($start >= $allLines) {
            return json(array("error" => 12, "message" => "No more relevant results."));
        }

        if ($end >= $allLines) {
            $end = $allLines;
            $count = $end - $start;
        }

        $similarityArray = array_reverse($similarityArray);

        // Limit them.
        $images = array();
        for ($i = 0; $i < $count; $i++) {
            $line = $similarityArray[$i];
            $id = substr($line, 0, strpos($line, "|"));
            $images[] = array(
                "id" => $id,
                "date" => 0
            );
        }

        return json($images);
    }

}