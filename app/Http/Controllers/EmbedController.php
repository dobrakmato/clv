<?php

namespace App\Http\Controllers;

use App\Libraries\ImageRepo;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * Provides embed-ability.
 *
 * @package App\Http\Controllers
 */
class EmbedController extends BaseController
{
    /**
     * Renders embed HTML page.
     *
     * @param Request $request request
     * @param int|string $image_id image id
     * @return \Illuminate\View\View view
     */
    public function embed(Request $request, $image_id)
    {
        // Decode ID.
        $image_id = image_id($image_id);

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }

        if (ImageRepo::exists($image_id)) {
            $url = image_link($request, $image_id);
            // Render embed view.
            return view("embed", array("image_id" => $image_id, "url" => $url));
        } else {
            abort(404);
        }
    }
}