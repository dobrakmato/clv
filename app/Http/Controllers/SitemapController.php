<?php
namespace App\Http\Controllers;

use App\Libraries\ImageRepo;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Used to generate sitemap containing all images in gallery + some static pages.
 *
 * @package App\Http\Controllers
 */
class SitemapController extends BaseController
{
    /**
     * Generates text file sitemap.
     *
     * @return string sitemap contents
     */
    public function getTxt()
    {
        $sitemap = "";

        // Include standard URLs.
        $sitemap .= "https://www.coolovo.eu" . "\r\n";
        $sitemap .= "https://www.coolovo.eu/latest" . "\r\n";
        $sitemap .= "https://www.coolovo.eu/best" . "\r\n";
        $sitemap .= "https://www.coolovo.eu/random" . "\r\n";
        $sitemap .= "https://www.coolovo.eu/about" . "\r\n";
        $sitemap .= "https://www.coolovo.eu/blog" . "\r\n";

        // Include all images.
        $allFiles = ImageRepo::get(0, PHP_INT_MAX, true);

        foreach ($allFiles as $file) {
            $sitemap .= "https://www.coolovo.eu/" . ImageRepo::id($file) . "\r\n";
        }

        return $sitemap;
    }
}