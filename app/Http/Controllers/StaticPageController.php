<?php

namespace App\Http\Controllers;

use App\Libraries\ImageRepo;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Used by HTML routes.
 * @package App\Http\Controllers
 */
class StaticPageController extends BaseController
{
    /**
     * Whether the server should generate short (base56 encoded) urls in HTML.
     * @var bool
     */
    private static $SHORT_IDS = true;

    // TODO: Refactor HTML code generation.

    public function index(Request $request)
    {
        // If is referrer specified.
        if (isset($_GET["ref"])) {
            $refferal = $_GET["ref"];

            $obj = json_decode(file_get_contents(storage_path("refferals.txt")));

            if (isset($obj->$refferal)) {
                $obj->$refferal++;
            } else {
                $obj->$refferal = 1;
            }

            file_put_contents(storage_path("refferals.txt"), json_encode($obj, JSON_PRETTY_PRINT));
        }

        // Render latest posts page.
        return $this->latest($request);
    }

    /**
     * Returns HTML page with list of latest images.
     *
     * @param Request $request request
     * @return \Illuminate\View\View view
     */
    public function latest(Request $request)
    {
        $files = ImageRepo::get(0, 30);

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }

        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $serverRendered = "";
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $url = image_link($request, $id, IMAGE_SIZE_THUMBNAIL);

            $linkId = $id;
            if (self::$SHORT_IDS) {
                $linkId = base56_encode(intval($id));
            }

            $serverRendered .= '<div class="outer ns" id="' . $id . '"><a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/' . $linkId .
                '" class="thumb" style="background-image: url(' . $url . ');"></a></div>';
        }

        return view("list", array("category" => "najnovšie", "streamPath" => "/api/stream/latest", "galleryHtml" => $serverRendered));
    }

    /**
     * Returns HTML page with list of best images.
     *
     * @param Request $request request
     * @return \Illuminate\View\View view
     */
    public function best(Request $request)
    {
        $files = ImageRepo::get(0, 30);

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }

        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $serverRendered = "";
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $url = image_link($request, $id, IMAGE_SIZE_THUMBNAIL);

            $linkId = $id;
            if (self::$SHORT_IDS) {
                $linkId = base56_encode(intval($id));
            }

            $serverRendered .= '<div class="outer ns" id="' . $id . '"><a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/' . $linkId .
                '" class="thumb" style="background-image: url(' . $url . ');"></a></div>';
        }

        return view("list", array("category" => "najlepšie", "streamPath" => "/api/stream/best", "galleryHtml" => $serverRendered));
    }

    /**
     * Returns HTML page with list of user liked images.
     *
     * @param Request $request request
     * @return \Illuminate\View\View view
     */
    public function liked(Request $request)
    {
        $files = ImageRepo::get(0, 30);

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $serverRendered = "";
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $url = image_link($request, $id, IMAGE_SIZE_THUMBNAIL);

            if (self::$SHORT_IDS) {
                $id = base56_encode(intval($id));
            }

            $serverRendered .= '<div class="outer ns" id="' . $id . '"><a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/' . $id .
                '" class="thumb" style="background-image: url(' . $url . ');"></a></div>';
        }

        return view("list", array("category" => "obľúbené", "streamPath" => "/api/stream/liked", "galleryHtml" => $serverRendered));
    }

    /**
     * Returns HTML with loaded image specified in URL.
     *
     * @param Request $request request
     * @param int|string $image_id image id
     * @return \Illuminate\View\View view
     */
    public function permalink(Request $request, $image_id)
    {
        // Check if the id is numerical ID or encoded id.
        $image_id = image_id($image_id);

        if (!ImageRepo::exists($image_id)) {
            abort(404);
        }

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }

        // We ues less images to permalinks to save from bandwidth.
        $files = ImageRepo::get(0, 12);

        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $serverRendered = "";
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $url = image_link($request, $id, IMAGE_SIZE_THUMBNAIL);

            $linkId = $id;
            if (self::$SHORT_IDS) {
                $linkId = base56_encode(intval($id));
            }

            $serverRendered .= '<div class="outer ns" id="' . $id . '"><a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/' . $linkId .
                '" class="thumb" style="background-image: url(' . $url . ');"></a></div>';
        }

        return view("permalink", array("image_id" => $image_id, "streamPath" => "/api/stream/latest",
            "galleryHtml" => $serverRendered, "category" => "vtipný obrázok #" . $linkId));
    }

    /**
     * Displays static "About" page.
     *
     * @return \Illuminate\View\View view
     */
    public function about()
    {
        return view("about", array("category" => "čo to je?"));
    }

    /**
     * Creates a random stream.
     */
    public function random()
    {
        // Create random seed.
        $seed = mt_rand(pow(2, 24), pow(2, 28));
        // Return redirect.
        header("Location: /random/" . base56_encode(intval($seed)), true, 302);
        exit;
    }

    /**
     * Outputs random stream.
     *
     * @param Request $request request
     * @param int $seed stream shuffle seed
     * @return \Illuminate\View\View view
     */
    public function doRandom(Request $request, $seed)
    {
        $files = ImageRepo::getBySeed(0, 30, base56_decode($seed));

        // Create access token.
        if (!$request->session()->has("token")) {
            $request->session()->put("token", rtrim(base64_encode(md5(microtime())), "="));
        }

        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $serverRendered = "";
        foreach ($files as $file) {
            $id = ImageRepo::id($file);
            $url = image_link($request, $id, IMAGE_SIZE_THUMBNAIL);

            $linkId = $id;
            if (self::$SHORT_IDS) {
                $linkId = base56_encode(intval($id));
            }

            $serverRendered .= '<div class="outer ns" id="' . $id . '"><a href="' . $protocol . $_SERVER['HTTP_HOST'] . '/' . $linkId .
                '" class="thumb" style="background-image: url(' . $url . ');"></a></div>';
        }

        return view("list", array("category" => "náhodné", "streamPath" => "/api/stream/random/" . $seed, "galleryHtml" => $serverRendered));
    }
}
