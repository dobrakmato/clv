<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Minifies responses by removing white space and HTML comments from rendered
 * pages.
 *
 * @package App\Http\Middleware
 */
class MinifyHtmlMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        // Get original content.
        $output = $response->getOriginalContent();

        // If is the html string, perform minimization.
        if (str_contains($output, "<html")) {
            // Clean comments
            $output = preg_replace('/<!--([^\[|(<!)].*)/', '', $output);
            $output = preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $output);

            // Clean Whitespace
            $output = preg_replace('/\s{2,}/', '', $output);
            $output = preg_replace('/(\r?\n)/', '', $output);

            // Set new content to response object.
            $response->setContent($output);
        }


        return $response;
    }
}
