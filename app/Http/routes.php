<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
 * Index redirects.
 */
$redirect = function () {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: https://www.coolovo.eu/latest", true, 301);
    // Stop executing framework after sending redirect.
    exit;
};

$app->get('/index', $redirect);
$app->get('/index.php', $redirect);
$app->get('/index.html', $redirect);

/*
 * HTML Gallery routes.
 */
$app->get('/', 'StaticPageController@index');
$app->get('/latest', 'StaticPageController@latest');
$app->get('/best', 'StaticPageController@best');
$app->get('/liked', 'StaticPageController@liked');
$app->get('/about', 'StaticPageController@about');
$app->get('/search', 'StaticPageController@search');
$app->get('/random', 'StaticPageController@random');
$app->get('/random/{seed}', 'StaticPageController@doRandom');

/*
 * HTML Blog routes.
 */
$app->get('/blog', 'BlogController@index');
$app->get('/blog/{slug}', 'BlogController@post');

/*
 * HTML Authentication routes.
 */
$app->get('/login', 'LoginController@viewLogin');

/*
 * HTML Administration routes.
 */
$app->get('/admin/cache', 'AdminController@cache');
$app->get('/admin/dashboard', 'AdminController@dashboard');
$app->get('/admin/images', 'AdminController@images');
$app->get('/admin/log', 'AdminController@log');
$app->get('/admin/reports', 'AdminController@reports');
$app->get('/admin/stats', 'AdminController@stats');

/*
 * Embed code route.
 */
$app->get('/embed/{imageId}', 'EmbedController@embed');

/*
 * Sitemap route.
 */
$app->get('/sitemap.txt', 'SitemapController@getTxt');

/*
 * JSON API - stream endpoint routes.
 */
$app->get('/api/stream/latest', 'Api\StreamController@latest');
$app->get('/api/stream/best', 'Api\StreamController@best');
$app->get('/api/stream/liked', 'Api\StreamController@liked');
$app->get('/api/stream/search', 'Api\StreamController@search');
$app->get('/api/stream/random/{seed}', 'Api\StreamController@random');

/*
 * JSON API - image endpoint routes.
 */
$app->get('/api/image/{id}', 'Api\ImagesController@image');
$app->get('/api/image/{id}/details', 'Api\ImagesController@details');
$app->post('/api/image/{id}/like', 'Api\ImagesController@like');
$app->post('/api/image/{id}/dislike', 'Api\ImagesController@dislike');
$app->post('/api/image/{id}/view', 'Api\ImagesController@view');

/*
 * JSON API - other routes.
 */
$app->get('/api/report', 'Api\ReportController@report');
$app->get('/api/stats', 'Api\StatsController@viewStats');
$app->get('/api/token', 'Api\TokenController@current');

/*
 * HTML Gallery permalink route. This route has to be the last!
 *
 * This route has to be registered last, because it matches all
 * urls in format '/anything'. That provides nice way to have
 * short urls to images, but leave us to try to match this route
 * last.
 */
$app->get('/{imageId}', 'StaticPageController@permalink');