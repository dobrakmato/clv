<?php

/*
 * This provides faster alternative for virtual environments in opposite
 * to dotEnv.
 */

set_env_var("APP_ENV", "staging");
set_env_var("APP_DEBUG", "true");
set_env_var("APP_KEY", "6FAvAnGkpeMQ9Sz8rGNfmP1eIpDMwiHl");

set_env_var("APP_LOCALE", "en");
set_env_var("APP_FALLBACK_LOCALE", "en");

set_env_var("DB_CONNECTION", "mysql");
set_env_var("DB_HOST", "localhost");
set_env_var("DB_PORT", 3306);
set_env_var("DB_DATABASE", "reposter");
set_env_var("DB_USERNAME", "root");
set_env_var("DB_PASSWORD", "");

set_env_var("FORCE_HTTPS", "false");

set_env_var("IR_EXTENSION", "jpg");
set_env_var("IR_ROOT", "D:/Shared/test/processed");
set_env_var("IR_CACHE_LIFETIME", 300);

set_env_var("CACHE_DRIVER", "file");
set_env_var("SESSION_DRIVER", "file");
set_env_var("QUEUE_DRIVER", "database");
