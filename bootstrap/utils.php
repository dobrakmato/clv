<?php

// Some constants.
define('IMAGE_SIZE_LARGE', 'large');
define('IMAGE_SIZE_THUMBNAIL', 'thumbnail');

if (!function_exists('getallheaders')) {
    /**
     * Returns all headers as array.
     *
     * @return array array of all headers
     */
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

if (!function_exists('image_link')) {
    /**
     * Creates link to image resource using API endpoint.
     *
     * @param \Illuminate\Http\Request $request request
     * @param int|string $id id of image
     * @param string $size size of image (large, thumbnail)
     * @return string url of image
     */
    function image_link(\Illuminate\Http\Request $request, $id, $size = IMAGE_SIZE_LARGE)
    {
        $token = $request->session()->get('token');

        $protocol = $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        return $protocol . $_SERVER['HTTP_HOST'] . '/api/image/' . $id . '?size=' . $size . '&token=' . $token;
    }
}

if (!function_exists('get_class_name')) {
    /**
     * Returns simple name of specified class.
     *
     * @param $classname string class name.
     * @return bool|int|string simple class name
     */
    function get_class_name($classname)
    {
        if ($pos = strrpos($classname, '\\')) return substr($classname, $pos + 1);
        return $pos;
    }
}

if (!function_exists('env_var')) {
    /**
     * Returns environment variable.
     *
     * @param $key string name of environment variable
     * @param $default mixed default value to return if variable is not set
     * @return mixed value of specified environment variable or default value
     */
    function env_var($key, $default)
    {
        $val = isset($_ENV[$key]) ? $_ENV[$key] : null;
        if ($val == null) {
            $val = getenv($key);
            if ($val == false) {
                return $default;
            }
            return $val;
        }
        return $val;
    }
}

if (!function_exists('set_env_var')) {
    /**
     * Sets environment variable.
     *
     * @param $key string name of variable
     * @param $value string|mixed value of variable
     */
    function set_env_var($key, $value)
    {
        putenv("$key=$value");
        $_ENV[$key] = $value;
        $_SERVER[$key] = $value;
    }
}

if (!function_exists('image_id')) {
    /**
     * Decodes image id to integer if it is in base56 encoded string.
     *
     * @param $str_input int|string int of string id
     * @return int integer id of image
     */
    function image_id($str_input)
    {
        if (is_numeric($str_input)) {
            return intval($str_input);
        } else {
            if (preg_match('/[a-zA-Z2-9]+/', $str_input) > 0) {
                return base56_decode($str_input);
            } else {
                throw new RuntimeException("Invalid ID!");
            }
        }
    }
}

if (!function_exists('json')) {
    /**
     * Returns string of specified array encoded as JSON object.
     *
     * @param $array array
     * @return string json string of object defined by specified array
     */
    function json($array)
    {
        header('Content-Type: application/json');
        $object = new stdClass();
        foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return json_encode($object, JSON_PRETTY_PRINT);
    }
}

if (!function_exists('firstSentence')) {
    /**
     * Get the first sentence of a string.
     *
     * If no ending punctuation is found then $text will
     * be returned as the sentence. If $strict is set
     * to TRUE then FALSE will be returned instead.
     *
     * @param  string $text Text
     * @param  boolean $strict Sentences *must* end with one of the $end characters
     * @param  string $end Ending punctuation
     * @return string|bool     Sentence or FALSE if none was found
     */
    function firstSentence($text, $strict = false, $end = '.?!')
    {
        preg_match("/^[^{$end}]+[{$end}]/", $text, $result);
        if (empty($result)) {
            return ($strict ? false : $text);
        }
        return $result[0];
    }
}

// Alphabet used in base56 encoding.
$GLOBALS['alphabet'] = str_split("23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ");

if (!function_exists('base56_encode')) {

    /**
     * Encodes specified integer as base56 encoded string.
     *
     * @param int $num integer to encode
     * @return string base56 encoded
     * @throws Exception
     */
    function base56_encode($num)
    {
        if (!is_numeric($num)) {
            throw new Exception("num is not numeric!");
        }

        if ($num == 0) {
            return 0;
        }

        $n = str_split($num);
        $arr = array();
        $base = sizeof($GLOBALS['alphabet']);

        while ($num) {
            $rem = $num % $base;
            $num = (int)($num / $base);
            $arr[] = $GLOBALS['alphabet'][$rem];
        }

        $arr = array_reverse($arr);
        return implode($arr);
    }
}

if (!function_exists('base56_decode')) {

    /**
     * Decodes base56 encoded string to integer.
     *
     * @param $string string base56 encoded string
     * @return int integer value of this string
     * @throws Exception
     */
    function base56_decode($string)
    {
        if (!is_string($string)) {
            throw new Exception("num is not numeric!");
        }

        $base = sizeof($GLOBALS['alphabet']);
        $strlen = strlen($string);
        $num = 0;
        $idx = 0;

        $s = str_split($string);
        $tebahpla = array_flip($GLOBALS['alphabet']);

        foreach ($s as $char) {
            $power = ($strlen - ($idx + 1));
            $num += $tebahpla[$char] * (pow($base, $power));
            $idx += 1;
        }
        return $num;
    }
}