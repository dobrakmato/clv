<?php

require_once __DIR__ . '/../vendor/autoload.php';

/*
|-------------------------------------------------------------------------
| Load global utility methods.
|-------------------------------------------------------------------------
| Provides helper methods for base56 encoding, decoding and simillar
| stuff.
|
|*/
require_once("utils.php");

// We use env.php, because it's faster.
require_once("env.php");

/*
|-------------------------------------------------------------------------
| Check for 64 bits
|-------------------------------------------------------------------------
| Some versions of PHP don't support 64bit integers. We have to display
| at least a warning until we move to GMP or something!
|
|*/
if (PHP_INT_MAX < 9223372036854775807) {
    echo "<div style='position:fixed;top:1em;left:1em;right:1em;text-align:center;padding:1em;border: 2px solid red;z-index:99999999;
    background: pink; color: red;'><b>You are using version of PHP which doesn't support 64-bit integers. Please upgrade to 64-bit version of PHP!</b></div>";
}


/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

// $app->withFacades();

// $app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    Illuminate\Cookie\Middleware\EncryptCookies::class,
    Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
    Illuminate\Session\Middleware\StartSession::class,
    Illuminate\View\Middleware\ShareErrorsFromSession::class,
    App\Http\Middleware\MinifyHtmlMiddleware::class
    // Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
]);

// $app->routeMiddleware([

// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$config = array(
    // required
    'access_token' => '79ed8b8c8de2465faf3936b36c0a84df',
    // optional - environment name. any string will do.
    'environment' => 'production',
    // optional - path to directory your code is in. used for linking stack traces.
    'root' => '/usr/share/nginx/html/domains/coolovo.eu'
);
Rollbar::init($config);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__ . '/../app/Http/routes.php';
});

return $app;
