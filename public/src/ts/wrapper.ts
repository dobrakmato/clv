/// <reference path="component.ts" />
/// <reference path="references.ts" />

/**
 * Abstract class that wraps element in jQuery object.
 */
class Wrapper extends Component {
    public element:JQuery;

    constructor(selector:string) {
        super();
        this.element = $(selector);
    }
}