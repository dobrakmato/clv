/// <reference path="references.ts" />
/// <reference path="component.ts" />

class SoundEffect extends Component {

    private static URL = '//coolovo.eu/dist/notification.mp3';

    private audioElement:JQuery = null;

    constructor() {
        super();
    }

    public play():void {
        if (this.audioElement === null) {
            this.createAudioElement();
        }

        this.audioElement.trigger('play');
    }

    public getComponentName():string {
        return 'SoundEffect';
    }

    private createAudioElement():void {
        var el = $('<audio><source src="' + SoundEffect.URL + '" type="audio/mpeg" preload="auto"></audio>')
            .css('display', 'none');
        el.appendTo(document.body);
        this.audioElement = el;
    }
}