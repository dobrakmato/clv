class Preloader extends Component {

    private preloadRoot:JQuery;

    constructor() {
        super();
        this.createMainNode();
    }

    private createMainNode() {
        this.preloadRoot = $('<div/>')
            .attr('id', 'preload-root')
            .css('display', 'none')
            .appendTo(document.body);
    }

    public preloadImage(url:string) {
        var element = $('<img/>')
            .one('load', () => {
                element.remove();
            })
            .attr('src', url)
            .appendTo(this.preloadRoot);
    }

    public getComponentName():string {
        return 'Preloader';
    }
}