/// <reference path="constants.ts" />
/// <reference path="references.ts" />

/**
 * This class provides nice builder for Setting class.
 */
class SettingBuilder {
    private name:string;
    private description:string;
    private default:string;

    constructor(name:string) {
        this.name = name;
    }

    public withDefault(defaultValue:string):SettingBuilder {
        this.default = defaultValue;
        return this;
    }

    public withDescripton(description:string):SettingBuilder {
        this.description = description;
        return this;
    }

    public build():Setting {
        return new Setting(this.name, localStorage.getItem('settings.' + this.name) !== null ?
            localStorage.getItem('settings.' + this.name) : this.default, this.description);
    }
}

/**
 * Represents setting in application.
 */
class Setting {

    private static settingsPage:JQuery = $('#settings_holder');

    private name:string;
    private value:string;
    private description:string;

    constructor(name:string, value:string, description:string) {
        this.name = name;
        this.value = value;
        this.description = description;

        this.addToSettings();
    }

    private addToSettings() {

        if (this.asBoolean()) {
            var label = $('<label for="setting-' + this.name + '">' + '<input type="checkbox" name="setting-'
                + this.name + '" checked="checked"></input>' + this.description + '</label>');
        } else {
            var label = $('<label for="setting-' + this.name + '">' + '<input type="checkbox" name="setting-'
                + this.name + '"></input>' + this.description + '</label>');
        }
        label.children().first().click(() => {
            this.value = this.value === 'true' ? 'false' : 'true';
            this.save();
        });
        var div = $('<div/>');
        div.append(label);
        Setting.settingsPage.append(div);
    }

    public getName():string {
        return this.name;
    }

    public getDescription():string {
        return this.description;
    }

    public getValue():string {
        return this.value;
    }

    public asBoolean():boolean {
        return this.value === 'true';
    }

    public asInteger():number {
        return parseInt(this.value, 10);
    }

    public asFloat():number {
        return parseFloat(this.value);
    }

    public save() {
        localStorage.setItem('settings.' + this.name, this.value);
    }

    public static of(name:string):SettingBuilder {
        return new SettingBuilder(name);
    }
}

/**
 * Holds constants of all settings in application.
 */
class Settings {
    public static USE_ANIMATIONS:Setting = Setting
        .of('animations')
        .withDescripton('Use nice animations in the page.')
        .withDefault('true')
        .build();

    public static CHECK_FOR_NEW_POSTS:Setting = Setting
        .of('autocheck')
        .withDescripton('Automatically check for new posts.')
        .withDefault(Constants.IS_MOBILE ? 'false' : 'true') // This defaults to whether the platform is desktop.
        .build();

    public static PLAY_SOUND_NEW_POSTS:Setting = Setting
        .of('autocheck-sound')
        .withDescripton('Play sound effect when there are new posts.')
        .withDefault('true')
        .build();

    public static USE_SHORT_URLS:Setting = Setting
        .of('base56urls')
        .withDescripton('Use short URLs where possible.')
        .withDefault('true')
        .build();
}

// Initialize settings by adding settings button to navbar.
function bindSettingsButtons() {
    $('#settings-btn').click(() => {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            $('#overlay').velocity('transition.fadeIn', 300);
            $('#settings').velocity('transition.fadeIn', 300);
        } else {
            $('#overlay').css('display', 'block');
            $('#settings').css('display', 'block');
        }
    });

    $('#settings-close-btn').click(() => {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            $('#overlay').velocity('transition.fadeOut', 300);
            $('#settings').velocity('transition.fadeOut', 300);
        } else {
            $('#overlay').css('display', 'none');
            $('#settings').css('display', 'none');
        }
    });
}
bindSettingsButtons();