class Component {
    static app_ref:Application;

    app:Application;

    constructor() {
        this.app = Component.app_ref;
    }

    public getComponentName():string {
        throw new Error('Derived class does not override getComponentName() function!');
    }
}