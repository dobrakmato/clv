/// <reference path="bootstrap.ts" />
/// <reference path="component.ts" />
/// <reference path="map.ts" />

class Application {
    private components:Map<Component>;

    constructor() {
        this.components = new Map<Component>();
        Component.app_ref = this;
    }

    public getComponent<T extends Component>(type:T):T {
        var name:string = type.getComponentName();

        if (this.components.containsKey(name)) {
            return (<T> this.components.get(name));
        } else {
            throw new Error('This application has no component like "' + name + '" !');
        }
    }

    public addComponent(component:Component) {
        this.components.put(component.getComponentName(), component);
    }
}

// Boot that shit.
new Bootstrap;