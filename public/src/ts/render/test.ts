/// <reference path="element.ts" />

class A {
    a() {
        var gallery = select('gallery');

        for (var i = 0; i < 10; i++) {
            create('div')
                .attr('id', i)
                .attr('class', 'thumbnail')
                .appendTo(gallery);
        }


    }
}