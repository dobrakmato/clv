/// <reference path="attribute.ts" />
/// <reference path="renderable.ts" />

class Const {
    public static O:string = '<';
    public static C:string = '>';
    public static S:string = ' ';
    public static SL:string = '/';
    public static A:string = '"';
    public static E:string = '=';
}

interface IElement {
    append(element:IElement);
    appendTo(element:IElement);
    prepend(element:IElement);
    prependTo(element:IElement);
    html(html?:string);
    attr(name:string, value?:any):any;
}

/**
 * Wrapper for existing DOM element.
 */
class WrappedElement implements IElement {

    private ref:HTMLElement;

    constructor(ref:HTMLElement) {
        this.ref = ref;
    }

    public append(element:IElement) {
        this.ref.innerHTML = this.ref.innerHTML + element.html();
    }

    public prepend(element:IElement) {
        this.ref.innerHTML = element.html() + this.ref.innerHTML;
    }

    public appendTo(element:IElement) {
        element.append(this);
    }

    public prependTo(element:IElement) {
        element.prepend(this);
    }

    public html(html?:string) {
        if (html) {
            this.ref.innerHTML = html;
        } else {
            return this.ref.innerHTML;
        }
    }

    public attr(name:string, value?:any):any {
        if (value) {
            this.ref.setAttribute(name, value);
        } else {
            return this.ref.getAttribute(name);
        }
    }

}

/**
 * Element that has to be rendered and them appended to DOM element.
 */
class GeneratedElement implements IRenderable, IElement {
    private type:string;
    private attributes:Attribute[];
    private value:string;

    constructor(type:string) {
        this.type = type;
    }

    public append(element:IElement) {
        this.value = this.value + element.html();
    }

    public prepend(element:IElement) {
        this.value = element.html() + this.value;
    }

    public appendTo(element:IElement) {
        element.append(this);
    }

    public prependTo(element:IElement) {
        element.prepend(this);
    }

    public html(html?:string):any {
        if (html) {
            this.value = html;
            return this;
        } else {
            return html;
        }
    }

    public attr(name:string, value?:any):any {
        if (value) {
            for (var i = 0; i < this.attributes.length; i++) {
                if (this.attributes[i].name === name) {
                    this.attributes[i].value = value.toString();
                    return this;
                }
            }
        } else {
            for (var i = 0; i < this.attributes.length; i++) {
                if (this.attributes[i].name === name) {
                    return this.attributes[i].value;
                }
            }
        }
    }

    public render():string {
        var attributes:string = '';
        for (var i = 0; i < this.attributes.length; i++) {
            attributes += Const.S + this.attributes[i].name + Const.E + Const.A + this.attributes[i].value + Const.A;
        }

        var html:string = Const.O + this.type + attributes + Const.C + this.html +
            Const.O + Const.SL + this.type + Const.C;

        return html;
    }

}

/**
 * Creates new IElement with specified type.
 *
 * @param type type of new element.
 * @returns {GeneratedElement} instance of generated element
 */
function create(type:string):GeneratedElement {
    return new GeneratedElement(type);
}

/**
 * Selects an existing DOM element.
 *
 * @param id of element in dom.
 * @returns {WrappedElement} wrapper of this element
 */
function select(id:string):WrappedElement {
    return new WrappedElement(document.getElementById(id));
}