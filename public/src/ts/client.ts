/// <reference path="application.ts" />
/// <reference path="bootstrap.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="map.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />


/**
 * Provides wrapper around JSON calls to API.
 */
class Client extends Component {
    public static LOAD_COUNT:number = 30;

    private endpoint:string;
    private start:number;
    private end:number;

    private token:string;

    constructor() {
        super();
        this.start = Client.LOAD_COUNT;
        this.end = Client.LOAD_COUNT * 2;

        // Get the session token.
        setTimeout(() => {
            $.getJSON('/api/token', (json:any) => {
                if (json.token) {
                    this.token = json.token;
                }
            });
        }, 1000);
    }

    /**
     * Sets images / feed endpoint (latest, best, favorited).
     *
     * @param endpoint new endpoint to use
     * @param startFromZero whether to reset client's position
     */
    public setEndpoint(endpoint:string, startFromZero:boolean = false) {
        this.endpoint = endpoint;
        if (startFromZero) {
            this.start = 0;
            this.end = Client.LOAD_COUNT;
        } else {
            this.start = Client.LOAD_COUNT;
            this.end = Client.LOAD_COUNT * 2;
        }
    }

    /**
     * Returns current endpoint URL.
     *
     * @returns {string} current endpoint
     */
    public getEndpointURL():string {
        return this.endpoint;
    }

    /**
     * Returns current endpoint.
     *
     * @returns {string} current endpoint
     */
    public getEndpoint():string {
        if (this.endpoint.indexOf('random') > -1) {
            return 'random/' + this.endpoint.substr(this.endpoint.lastIndexOf('/') + 1);
        } else {
            return this.endpoint.substr(this.endpoint.lastIndexOf('/') + 1);
        }
    }

    /**
     * Returns object.
     */
    public more(callback:(json:any) => void):any {
        $.getJSON(this.getURL(), (data:any) => {
            // Update start and end values.
            var count:number = this.end - this.start;
            this.start = this.end;
            this.end += count;
            // Call callback.
            callback(data);
        });
    }

    /**
     * Requests details about specific image.
     *
     * @param id id of the image
     * @param callback function to call when data arrives
     */
    public details(id:string, callback:(json:any) => void) {
        $.getJSON(this.getDetailsURL(id), (data:any) => {
            // Call callback.
            callback(data);
        });
    }

    public getOneLatest(callback:(json:any) => void) {
        $.getJSON(this.endpoint + '?start=0&end=1', (data:any) => {
            // Call callback.
            callback(data);
        });
    }

    private getDetailsURL(id:string):string {
        return '/api/image/' + id + '/details';
    }

    /**
     * Returns URL for AJAX request based on current endpoint, start and end values.
     *
     * @returns {string} url which should client request next
     */
    private getURL():string {
        if (this.endpoint.indexOf('?') > -1) {
            return this.endpoint + '&start=' + this.start + '&end=' + this.end;
        } else {
            return this.endpoint + '?start=' + this.start + '&end=' + this.end;
        }
    }

    public getImageThumbnailURL(id:string):string {
        return '/api/image/' + id + '?size=thumbnail&token=' + this.token;
    }

    public getImageURL(id:string):string {
        return '/api/image/' + id + '?size=original&token=' + this.token;
    }

    public getComponentName():string {
        return 'Client';
    }
}