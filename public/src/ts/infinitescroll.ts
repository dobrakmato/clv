/// <reference path='component.ts' />
/// <reference path='grid.ts' />
/// <reference path='references.ts' />


/**
 * Class that checks for scrolling of element and when is user almost at the top, this executed specified callback
 * also it ensures that callback it executed only once (or until the execution flag 'fired' is not reseted
 * with reset() method).
 */
class InfiniteScroll extends Component {
    private scrollableElement:JQuery;
    private fired:boolean;
    private cb:() => void;

    constructor(preloadCallback?:() => void) {
        super();

        this.scrollableElement = $('#scrollable-content');

        this.fired = false;

        if (preloadCallback) {
            this.cb = preloadCallback;
        }

        // Bind scroll event.
        this.scrollableElement.scroll(() => {
            // When almost at the end of page.
            if (this.scrollableElement.scrollTop() > this.scrollableElement.prop('scrollHeight') -
                this.scrollableElement.height() - 300 && this.fired === false) {
                this.fired = true;
                // Invoke callback.
                this.cb();
            }
        });
    }

    public setCallback(callback:() => void) {
        this.cb = callback;
    }

    /**
     * Resets fired flag.
     */
    public reset() {
        this.fired = false;
    }

    public getComponentName():string {
        return 'InfiniteScroll';
    }
}