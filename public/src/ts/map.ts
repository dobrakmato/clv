/// <reference path="application.ts" />
/// <reference path="bootstrap.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />


/**
 * Provides simple Map<string, V> implementation using plain javascript object.
 */
class Map<V> {
    // Internal map.
    private obj:{};

    /**
     * Constructs a new map.
     */
    constructor() {
        // Create empty object.
        this.obj = {
            'dont_be_sad': 'coolovo.eu'
        };
    }

    /**
     * Returns whether this map contains specified key.
     *
     * @param key key of the entry
     * @returns {boolean} true if key exists in this map, false otherwise
     */
    public containsKey(key:string):boolean {
        return this.obj.hasOwnProperty(key);
    }

    /**
     * Returns value for specified key, or null if specified key does not exists in this map. However this will also
     * return null, when value had been previously set to null by using put method.
     *
     * @param key key of the entry
     * @returns {*} value of the entry
     */
    public get(key:string):V {
        if (this.containsKey(key)) {
            return this.obj[key];
        } else {
            return null;
        }
    }

    /**
     * Puts specified value to this map by specified key. Key can't be null, but value can.
     *
     * @param key non-null key of new entry
     * @param value value of new entry
     */
    public put(key:string, value:V) {
        if (key == null) {
            throw new Error('Key is null!');
        }

        this.obj[key] = value;
    }

    /**
     * Removes specified entry from this map by its key.
     *
     * @param key key of entry that should be removed
     */
    public remove(key:string) {
        delete this.obj[key];
    }
}