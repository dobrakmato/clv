/**
 * Class that holds useful constants.
 */
class Constants {
    public static IS_MOBILE:boolean = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    public static GALLERY_SLIDE_IN_LENGTH:number;
    public static VIEWER_CHANGE_LENGTH:number;
    public static VIEWER_SHOW_LENGTH:number;

    public static init() {
        if (Constants.IS_MOBILE) {
            Constants.GALLERY_SLIDE_IN_LENGTH = 500;
            Constants.VIEWER_CHANGE_LENGTH = 200;
            Constants.VIEWER_SHOW_LENGTH = 500;
        } else {
            Constants.GALLERY_SLIDE_IN_LENGTH = 500;
            Constants.VIEWER_CHANGE_LENGTH = 200;
            Constants.VIEWER_SHOW_LENGTH = 200;
        }
    }
}
// Init these constants as some are platform based.
Constants.init();