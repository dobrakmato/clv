/// <reference path="references.ts" />

/**
 * Provides easy way to deal with buttons in MessageBox.
 */
class ButtonCollection {

    // Reference to parent message box.
    private msgbox_parent:MessageBox;

    /**
     * Constructs new ButtonCollection of specified MessageBox.
     *
     * @param msgbox parent message box
     */
    constructor(msgbox:MessageBox) {
        this.msgbox_parent = msgbox;
    }

    /**
     * Adds a button with specified text, callback and optional class name to this MessageBox.
     *
     * @param text          text of the button
     * @param callback      callback, that will be executed, when button is pressed
     * @param className     optional name of class that this button should use (in addition to class btn)
     *
     * @returns {ButtonCollection} instance of itself for easy method chaining
     */
    public button(text:string, callback?:() => void, className:string = 'btn-normal'):ButtonCollection {
        var button = $('<div/>')
            .addClass('btn ' + className)
            .text(text)
            .click(callback);

        this.msgbox_parent.addToFooter(button);
        return this;
    }

    /**
     * Returns instance of MessageBox this button collection belongs to.
     *
     * @returns {MessageBox} instance of message box of this button collection
     */
    public parent():MessageBox {
        return this.msgbox_parent;
    }
}

/**
 * Provides easy way to create nice modal message boxes using only Javascript.
 */
class MessageBox {

    private static overlayElement:JQuery;

    // Reference to main element.
    private element:JQuery;
    // Message box title element.
    private titleElement:JQuery;
    // Message box content element.
    private contentElement:JQuery;
    // Message box buttons element.
    private buttonsElement:JQuery;
    // Buttons in this message box.
    private _buttons:ButtonCollection;

    /**
     * Creates a new MessageBox object and DOM elements.
     *
     * @param title     title of message box
     * @param content   content of message box
     */
    constructor(title:string, content:string) {
        this._buttons = new ButtonCollection(this);

        // Create DOM for this message box.
        this.createDOM(title, content);
        // Initialize overlay.
        this.initializeOverlay();
    }

    /**
     * Shows this message box and global overlay with specified fade out animation.
     *
     * @param velocity  velocity ui pack animation name or javascript object with properties
     *                  that should be animated on message box's element
     * @param lenght    length of animation in milliseconds
     */
    public show(velocity:any, lenght:number = 300) {
        // Show overlay.
        MessageBox.overlayElement.velocity('fadeIn', lenght);
        // Show message box.
        this.element.velocity(velocity, lenght);
    }

    /**
     * Hides this message box and global overlay with specified fade out animation.
     *
     * @param velocity  velocity ui pack animation name or javascript object with properties
     *                  that should be animated on message box's element
     * @param lenght    length of animation in milliseconds
     */
    public hide(velocity:any, lenght:number = 300) {
        // Hide message box.
        this.element.velocity(velocity, lenght);
        // Hide overlay.
        MessageBox.overlayElement.velocity('fadeOut', lenght);
    }

    /**
     * Returns collection of buttons in this message box. This collection can be used to create more buttons.
     *
     * @returns {ButtonCollection} collection of all buttons this message box has
     */
    public buttons():ButtonCollection {
        return this._buttons;
    }

    /**
     * Adds specified element to footer of this MessageBox. This is used internally to allow ButtonCollection
     * accessing footer (button part) of this message box.
     *
     * @param element   element that should be added
     */
    public addToFooter(element:JQuery) {
        this.buttonsElement.append(element);
    }

    /**
     * This function creates needed DOM elements to display MessageBox and inserts content and title to them.
     *
     * @param title     title of message box
     * @param content   content of message box
     */
    private createDOM(title:string, content:string) {
        // Create needed elements.
        this.titleElement = $('<div/>')
            .addClass('msgbox-title')
            .text(title);
        this.contentElement = $('<div/>')
            .addClass('msgbox-content')
            .html(content);
        this.buttonsElement = $('<div/>')
            .addClass('msgbox-buttons');

        // Create main element and add children.
        this.element = $('<div/>')
            .addClass('msgbox')
            .append(this.titleElement)
            .append(this.contentElement)
            .append(this.buttonsElement);
    }

    /**
     * Tries to find overlay element in document's DOM, if no overlay element was found this will create
     * a new overlay element and use it as overlay.
     */
    private initializeOverlay() {
        // If we already have overlay element for our message boxes, don't initialize anything.
        if (MessageBox.overlayElement != null) {
            return;
        }

        // Try to find overlay by id.
        var overlay = $('#overlay');
        // If this failed, and no overlay by id was found, try instead class.
        if (overlay.length === 0) {
            overlay = $('.overlay');
        }
        // Check if this failed again, if so, create an overlay element.
        if (overlay.length === 0) {
            overlay = $('<div/>')
                .attr('id', 'overlay')
                .addClass('overlay');
            // Assign created overlay to static reference.
            MessageBox.overlayElement = overlay;
        }
    }
}