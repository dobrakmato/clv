var alphabet:string[] = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'.split('');
var tebahpla:string[] = alphabet.slice().reverse();

/**
 * Converts specified number to base56 encoded string.
 *
 * @param num number to encode.
 * @returns {any} base56 string representation of that number
 */
function base56_encode(num):string {
    if (num === 0) {
        return '0';
    }
    var arr:string[] = [];
    while (num > 0) {
        var rem = num % 56;
        num = Math.floor(num / 56);
        arr.push(alphabet[rem]);
    }
    arr = arr.reverse();
    return arr.join('');
}

/**
 * Converts base56 string encoded number to integer value.
 *
 * @param str base56 string encoded number
 * @returns {number} integer value
 */
function base56_decode(str:string) {
    var num = 0;
    var idx = 0;
    for (var i = 0; i < str.length; i++) {
        var char = str.charAt(i);
        var power = (str.length - (idx + 1));
        num += tebahpla.indexOf(char) * (Math.pow(56, power));
        idx += 1;
    }
    return num;
}