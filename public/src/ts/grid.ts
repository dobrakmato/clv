/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="references.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />
/// <reference path="preloader.ts" />

/**
 * Provides wrapper around image 'grid' on page.
 */
class Grid extends Wrapper {

    public static IMAGE_ID_ATTR:string = 'id';

    private client:Client;
    private scrollableElement:JQuery;
    private preloader:Preloader;

    constructor() {
        super('#gallery');

        this.scrollableElement = $('#scrollable-element');
        this.client = this.app.getComponent(Client.prototype);
        this.preloader = this.app.getComponent(Preloader.prototype);

        this.registerTouchEvents();
        this.preloadFirst();
    }

    private preloadFirst():void {
        // Preload large version of first image.
        this.preloader.preloadImage(this.client.getImageURL(this.firstId()));
    }

    public registerTouchEvents() {
        // TODO: Make pull and release to refresh.
        this.scrollableElement.on('swipeDown', () => {
            if (this.scrollableElement.scrollTop() < 10) {
                this.clear(() => {
                    $('#spinner').show();
                    window.location = window.location;
                });
            }
        });
    }

    /**
     * Plays animation and shows grid.
     */
    public show() {
        if (!Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.css('display', 'block');
            return;
        }

        // Mobile browsers are slower, use easier animation for them.
        if (Constants.IS_MOBILE) {
            // Fade in all items.
            this.element.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
        } else {
            // Fade in and move up all items.
            this.element.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
        }
    }

    /**
     * Plays animation and hides grid.
     */
    public clear(callback?:() => void) {
        // Fade out and move bottom all items.
        if (!Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.empty();
            if (callback) {
                callback();
            }
            return;
        }

        var animation = 'transition.slideDownOut';
        if (Constants.IS_MOBILE) {
            animation = 'transition.fadeOut';
        }

        this.element.velocity(animation, Constants.GALLERY_SLIDE_IN_LENGTH, () => {
            // Remove all items from DOM.
            this.element.empty();
            if (callback) {
                callback();
            }
        });
    }

    /**
     * Returns current number of elements (images - thumbnails) in grid.
     *
     * @returns {number} amount of images in grid
     */
    public size():number {
        return this.element.children().length;
    }

    /**
     * Returns index of specified image in grid by its id.
     *
     * @param id id of the image
     * @returns {number} index of the image
     */
    public indexOf(id:string):number {
        return $('#' + id).index();
    }

    /**
     * Returns ID of the first photo in grid (probably the newest when in latest endpoint).
     *
     * @returns id of the first image in the grid.
     */
    public firstId():string {
        return this.getImageIdFromElement(this.element.children().first());
    }

    /**
     * Returns id, that goes after specified id in the grid. If there is no more images after
     * specified one, this tries to load more from server. If the server provide empty result
     * this returns last image in the grid. Otherwise it appends loaded content to the grid
     * and returns next image id.
     *
     * @param id starting id
     * @returns {string} id after starting id
     */
    public getAfter(id:string):string {
        // If passed id is undefined (this happens usually when no image
        // was displayed, because page was just loaded) return first image
        // in the grid.
        if (id === undefined) {
            console.error('Current image is undefined!');
            return this.getImageIdFromElement(this.element.children().first());
        }
        // Get index of current image in the grid.
        var index:number = $('#' + id).index();
        // If user requested more, then we currently have, try to load more
        // otherwise return last.
        if (this.size() > index + 1) {
            return this.getImageIdFromElement(this.element.children().eq(index + 1));
        } else {
            // Currently we only return last ID.
            // TODO: Make this perform load more.
            console.error('Current index is: ' + index + ', size: ' + this.size() + ', but user requested more!');
            return id;
        }
    }

    /**
     * Returns id, that goes before specified id in the grid. If there is no more items before
     * specified one, this returns first image in the grid.
     *
     * @param id starting id
     * @returns {string} id before starting id
     */
    public getBefore(id:string):string {
        // If passed id is undefined (this happens usually when no image
        // was displayed, because page was just loaded) return first image
        // in the grid.
        if (id === undefined) {
            console.error('Current image is undefined!');
            return this.getImageIdFromElement(this.element.children().first());
        }
        // Get index of current image in the grid.
        var index:number = $('#' + id).index();
        // If user requested less, then zero, return first id.
        if (0 <= index - 1) {
            return this.getImageIdFromElement(this.element.children().eq(index - 1));
        } else {
            console.error('Current index is: ' + index + ', size: ' + this.size() + ', but user requested less!');
            return this.getImageIdFromElement(this.element.children().first());
        }
    }

    // Returns actual ID from element.
    private getImageIdFromElement(element:JQuery):string {
        // Maybe we should use strings instead?
        return element.attr(Grid.IMAGE_ID_ATTR);
    }

    /**
     * Appends items stored in JSON to grid.
     * @param json json
     */
    public append(json:any) {
        // For each value in JSON.
        for (var key in json) {
            if (json.hasOwnProperty(key)) {
                var item = json[key];
                var image_url = this.client.getImageThumbnailURL(item.id);
                // Add to DOM.
                var innerElement:JQuery = $('<a/>')
                    .attr('href', '//' + window.location.hostname + '/' + item.id)
                    .addClass('thumb')
                    .css('background-image', 'url(' + image_url + ')');
                var outerElement:JQuery = $('<div/>')
                    .addClass('outer ns')
                    .attr(Grid.IMAGE_ID_ATTR, item.id);

                this.element.append(outerElement.append(innerElement));
            }
        }
    }

    public prependElement(el:JQuery) {
        this.element.prepend(el);
    }

    public getComponentName():string {
        return 'Grid';
    }
}