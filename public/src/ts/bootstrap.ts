/// <reference path="application.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="map.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="preloader.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="soundeffect.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />

declare var dontBoot;
declare var streamPath:string;

class Bootstrap {
    public app:Application;

    constructor() {
        // Create application
        this.app = new Application();
        // Fix after 7 hours of debugging.
        Component.app_ref = this.app;

        // Add components.
        this.app.addComponent(new Preloader());
        this.app.addComponent(new Client());
        this.app.addComponent(new Spinner());
        this.app.addComponent(new Grid());
        this.app.addComponent(new ImageViewer());
        this.app.addComponent(new ScrollToTop());
        this.app.addComponent(new InfiniteScroll());
        this.app.addComponent(new SoundEffect());

        // Configure components.
        this.installInfiniteScroll();

        // Ajaxify page (links, etc...).
        this.ajaxify();

        // Disable scrollbars when using on mobile.
        Bootstrap.installMobileFixes();


        if (dontBoot === true) {
            this.app.getComponent(Spinner.prototype).hide();
            this.app.getComponent(Grid.prototype).show();

            var el:JQuery = $('#scrollable-content');

            // This is hacky.
            if (!Settings.USE_ANIMATIONS.asBoolean()) {
                el.css('display', 'block');
                // FIX: Don't forge to bind thumb clicks before returning.
                this.bindThumbClick();
                return;
            }

            // Mobile browsers are slower, use easier animation for them.
            if (Constants.IS_MOBILE) {
                // Fade in all items.
                el.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            } else {
                // Fade in and move up all items.
                el.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }

            return;
        }

        // Bind all keyboard shortcuts.
        this.bindKeyboard();

        // Install new posts checker.
        this.installNewPostsChecker();

        this.installBackButton();

        // Set endpoint to latest images.
        if (typeof streamPath === 'undefined') {
            // Fallback to default stream.
            console.error('Stream endpoint not provided!');
            this.app.getComponent(Client.prototype).setEndpoint('api/stream/latest');
        } else {
            // Use provided stream.
            this.app.getComponent(Client.prototype).setEndpoint(streamPath);
        }


        setTimeout(() => {
            this.app.getComponent(Spinner.prototype).hide();
            this.app.getComponent(Grid.prototype).show();

            var el:JQuery = $('#scrollable-content');

            // This is hacky.
            if (!Settings.USE_ANIMATIONS.asBoolean()) {
                el.css('display', 'block');
                // FIX: Don't forge to bind thumb clicks before returning.
                this.bindThumbClick();
                return;
            }

            // Mobile browsers are slower, use easier animation for them.
            if (Constants.IS_MOBILE) {
                // Fade in all items.
                el.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            } else {
                // Fade in and move up all items.
                el.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }

            // Activate viewer transparency on PCs.
            if (!Constants.IS_MOBILE) {
                $(document.body).append($('<style>#viewer{background:rgba(0,0,0,0.92);}</style>'));
            }

            this.bindThumbClick();
        }, 300);
    }

    // Other bootstrap methods are here.

    private installNewPostsChecker() {
        if (Settings.CHECK_FOR_NEW_POSTS.asBoolean()) {
            var task_id = setInterval(() => {
                this.app.getComponent(Client.prototype).getOneLatest((json:any) => {
                    var localFirstId = this.app.getComponent(Grid.prototype).firstId();
                    if (localFirstId !== json[0].id) {
                        // There are new posts.
                        var message = $('<div class="centered"><div class="newposts">'
                            + 'New posts! Refresh the page to see new images!</div></div>');
                        message.click(() => {
                            // TODO: AJAX refresh.
                            window.location = window.location;
                        });

                        // Prepend element.
                        this.app.getComponent(Grid.prototype).prependElement(message);
                        // Change title.
                        window.document.title = '[NEW!] ' + window.document.title;

                        // Play sound effect is requested.
                        if (Settings.PLAY_SOUND_NEW_POSTS.asBoolean()) {
                            this.app.getComponent(SoundEffect.prototype).play();
                        }

                        // Stop from further execution.
                        clearInterval(task_id);
                    }
                });
            }, 30000);
        }
    }

    private installInfiniteScroll() {
        var infiniteScroll = this.app.getComponent(InfiniteScroll.prototype);
        infiniteScroll.setCallback(() => {
            this.app.getComponent(Client.prototype).more((json:any) => {
                // If we got empty object, we are at the end of images or
                // something bad happened.
                if (!jQuery.isEmptyObject(json)) {
                    // Append data.
                    this.app.getComponent(Grid.prototype).append(json);
                    // Fix event handlers.
                    this.bindThumbClick();
                    // Reset scroll to allow further fetching.
                    infiniteScroll.reset();
                }
            });
        });
    }

    private static installMobileFixes() {
        if (Constants.IS_MOBILE) {
            // Install scrollbar fix.
            $(document.body).append($('<style>::-webkit-scrollbar{display:none;}' +
                '.thumb{background-size:contain; background-repeat:no-repeat;}</style>'));
            // Change layout a little bit.
            $(document.body).append($('<style>.thumb{display:block;background-size:contain;' +
                'background-repeat:no-repeat;height:25em;width:initial;}.outer{padding:0;display:block;}</style>'));
        }
    }

    public bindThumbClick() {
        var self = this;
        $('.outer').click(function (e:JQueryEventObject) {
            // Prevent navigation on links.
            e.preventDefault();

            self.app.getComponent(ImageViewer.prototype).setImage($(this).attr('id'));
            self.app.getComponent(ImageViewer.prototype).open();
        });
    }

    /**
     * Binds events on links on page to prevent from navigating and then uses AJAX to load content.
     */
    public ajaxify() {
        // Fake AJAX load, hehe...
        $('#navbar a').click(() => {
            // Hide content and show spinner.
            this.app.getComponent(Grid.prototype).element.hide();
            this.app.getComponent(Spinner.prototype).show();
        });
    }

    /**
     * Binds keyboards shortcuts currently using Mousetrap.js.
     */
    public bindKeyboard() {
        Mousetrap.bind(['o'], () => {
            this.app.getComponent(ImageViewer.prototype).open();
        });

        // Close image viewer.
        Mousetrap.bind(['esc', 'q'], () => {
            this.app.getComponent(ImageViewer.prototype).close();
        });
        // Prev image.
        Mousetrap.bind(['left', 'a'], () => {
            this.app.getComponent(ImageViewer.prototype).prev();
        });
        // Next image.
        Mousetrap.bind(['right', 'd', 'space'], () => {
            this.app.getComponent(ImageViewer.prototype).next();
        });
        // Like image.
        Mousetrap.bind(['l'], () => {
            this.app.getComponent(ImageViewer.prototype).like();
        });

        Mousetrap.bind('ctrl+y', () => {
            $(document.body).html('<iframe src="https://www.youtube.com/embed/6Z4vTWS0jHs" frameborder="0" ' +
                'style="height: 100%;width: 100%;" allowfullscreen></iframe>');
        });

        // Experimental search.
        Mousetrap.bind(['s e a r c h', 'f3', 'ctrl+f'], () => {
            var terms = prompt('Search terms:');
            // Set search endpoint and reset couter.
            this.app.getComponent(Client.prototype).setEndpoint('api/stream/search?q=' + terms, true);
            this.app.getComponent(Grid.prototype).clear(() => {
                this.app.getComponent(Client.prototype).more((json:any) => {
                    this.app.getComponent(Grid.prototype).append(json);
                    this.app.getComponent(Grid.prototype).show();
                    this.bindThumbClick();
                });
            });
            return false;
        });
    }

    private installBackButton():void {
        // Register navigation.
        window.onpopstate = () => {
            console.log('onNavigate');
            console.log('navigating back...');
            if (this.app.getComponent(ImageViewer.prototype).isOpened()) {
                console.log('closing opened image viewer');
                this.app.getComponent(ImageViewer.prototype).close();
            }
        };
    }
}
