/// <reference path="b56.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="references.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="wrapper.ts" />
/// <reference path="preloader.ts" />

// Var that is declared when using direct link to specific image.
declare var sharedImage:string;

/**
 * Wraps access to image viewer element and provides method to modify it.
 */
class ImageViewer extends Wrapper {

    private imageElement:JQuery;
    private titleElement:JQuery;
    private likesCountElement:JQuery;
    private dislikesCountElement:JQuery;
    private imageDateElement:JQuery;
    private imageTranscriptElement:JQuery;
    private imageOriginalLink:JQuery;
    private currentImage:string;
    private grid:Grid;
    private client:Client;
    private preloader:Preloader;

    private animationScore:number;

    constructor() {
        super('#viewer');

        this.grid = this.app.getComponent(Grid.prototype);
        this.client = this.app.getComponent(Client.prototype);
        this.preloader = this.app.getComponent(Preloader.prototype);

        // Set up child elements.
        this.imageElement = this.element.children('.viewer-image');
        this.titleElement = this.element.find('.viewer-title');
        this.likesCountElement = this.element.find('.like-count');
        this.dislikesCountElement = this.element.find('.dislike-count');
        this.imageDateElement = $('#image-date');
        this.imageTranscriptElement = $('#image-transcript');
        this.imageOriginalLink = $('#image-original');

        this.animationScore = 0;

        this.registerTouchEvents();

        // Bind clicks of buttons.
        this.element.find('#like-btn').click(() => {
            this.like();
        });
        this.element.find('#dislike-btn').click(() => {
            this.dislike();
        });
        this.element.find('#fshare-btn').click(() => {
            this.facebookShare();
        });
        this.element.find('#twittershare-btn').click(() => {
            this.twitterShare();
        });
        this.element.find('#download-btn').click(() => {
            this.download();
        });
        this.element.find('#report-btn').click(() => {
            var msg = prompt('Aký je problém s týmto obrázkom?');
            if (msg !== '' && msg != null && msg.length > 3) {
                // Send warning.
                $.getJSON('/api/report?id=' + this.currentImage + '&msg=' + msg, (data:any) => {
                    alert(data.status);
                });
            } else {
                alert('Ak chcete tento obrazok nahlasit, musite zadat spravu, ktora bude dlhsia ako 3 znaky.');
            }
        });
        this.element.find('#close-btn').click((e:JQueryEventObject) => {
            this.close();
            // Do not execute 'next' command.
            e.stopImmediatePropagation();
        });

        this.imageElement.click((e:JQueryEventObject) => {
            var f = e.pageX / $(document.body).width();
            if (f > 0.5) {
                this.next();
            } else {
                this.prev();
            }
        });

    }

    /**
     * Registers touch events.
     */
    public registerTouchEvents() {
        this.element.on('swipeLeft', () => {
            this.next();
        });
        this.element.on('swipeRight', () => {
            this.prev();
        });
        this.element.on('swipeDown', () => {
            if (this.element.scrollTop() < 5) {
                this.close();
            }
        });
        this.element.on('doubleTap', () => {
            this.like();
        });
    }

    /**
     * Returns whether the image viewer is opened at this moment.
     */
    public isOpened():boolean {
        // Return isVisible.
        return this.element.css('top') === '0px';
    }

    /**
     * Plays animation and hides (closes) image viewer.
     */
    public close() {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.velocity({'top': '100%'}, Constants.VIEWER_SHOW_LENGTH);
        } else {
            this.element.css('top', '100%');
        }
        // Update URL.

        // If permalink use permalink as URL when not viewing image.
        if (typeof sharedImage !== 'undefined') {
            var id = Settings.USE_SHORT_URLS.asBoolean() ? base56_encode(sharedImage) : sharedImage;
            window.history.pushState({}, '', window.location.protocol + '//' + window.location.host + '/' + id);
        } else {
            // Make use of endpoints. this.client.getEndpointURL()
            window.history.pushState({}, '', window.location.protocol + '//' + window.location.host + '/' + this.client.getEndpoint());
        }
    }

    /**
     * Plays animation and shows (opens) image viewer.
     */
    public open() {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.velocity({'top': '0%'}, Constants.VIEWER_SHOW_LENGTH);
        } else {
            this.element.css('top', '0%');
        }
    }

    // Animates to next image by its id.
    private animateNext(newId:string) {
        this.animationScore++;
        // Fade out. {'opacity': '0.0'} 'transition.slideLeftOut'
        this.imageElement.velocity({'opacity': '0.0'}, Constants.VIEWER_CHANGE_LENGTH, () => {
            this.setImage(newId);
            // Fade in. {'opacity': '1.0'} 'transition.slideRightIn'
            this.imageElement.velocity({'opacity': '1.0'}, Constants.VIEWER_CHANGE_LENGTH, () => {
                this.animationScore = 0;
            });
        });
    }

    // Animates to prev image by its id.
    private animatePrev(newId:string) {
        this.animationScore++;
        // Fade out. {'opacity': '0.0'} 'transition.slideRightOut'
        this.imageElement.velocity({'opacity': '0.0'}, Constants.VIEWER_CHANGE_LENGTH, () => {
            this.setImage(newId);
            // Fade in. {'opacity': '1.0'} 'transition.slideLeftIn'
            this.imageElement.velocity({'opacity': '1.0'}, Constants.VIEWER_CHANGE_LENGTH, () => {
                this.animationScore = 0;
            });
        });
    }

    /**
     * Makes viewer display next image in grid.
     */
    public next() {
        // Only if the image viewer is active.
        if (this.isOpened()) {
            // Check if we are at the end of grid.
            if (this.grid.indexOf(this.currentImage) === this.grid.size() - 3) {
                this.client.more((json:any) => {
                    this.grid.append(json);
                });
            }


            // Find new current image id.
            var nextId = this.grid.getAfter(this.currentImage);
            if (this.currentImage !== nextId) {
                // Animate only when animation score is less then one.
                // This makes going thought lot of images by holding arrow
                // easier and less laggy.
                if (this.animationScore < 1 && Settings.USE_ANIMATIONS.asBoolean()) {
                    this.animateNext(nextId);
                } else {
                    this.setImage(nextId);
                }
            }
        }
    }


    /**
     * Makes viewer display previous image in grid.
     */
    public prev() {
        // Only if the image viewer is active.
        if (this.isOpened()) {
            // Find new current image id.
            var prevId = this.grid.getBefore(this.currentImage);
            if (this.currentImage !== prevId) {
                // Animate only when animation score is less then one.
                // This makes going thought lot of images by holding arrow
                // easier and less laggy.
                if (this.animationScore < 1 && Settings.USE_ANIMATIONS.asBoolean()) {
                    this.animatePrev(prevId);
                } else {
                    this.setImage(prevId);
                }
            }
        }
    }

    /**
     * Actually sets image without any animation and loads new content to UI components.
     * @param id id of image to set
     */
    public setImage(id:string) {
        this.currentImage = id;

        // Load new image and get url from image proxy.
        this.imageElement.css('background-image', 'url(' + this.client.getImageURL(id) + ')');

        // Clear old values from components.
        this.titleElement.text(id);
        this.likesCountElement.text('0');
        this.dislikesCountElement.text('0');

        // Update URL.
        if (Settings.USE_SHORT_URLS.asBoolean()) {
            var newUrl = window.location.protocol + '//' + window.location.host + '/' + base56_encode(id);
            window.history.pushState({}, '', newUrl);
        } else {
            var newUrl = window.location.protocol + '//' + window.location.host + '/' + id;
            window.history.pushState({}, '', newUrl);
        }

        // Preload next image.
        this.preloader.preloadImage(this.client.getImageURL(this.grid.getAfter(id)));

        // Insert original link.
        var originalUrl:string = 'https://facebook.com/' + id;
        this.imageOriginalLink.text(originalUrl).attr('href', originalUrl);

        // Load new values.
        this.client.details(this.currentImage, (json) => {
            this.likesCountElement.text(json.likes);
            this.dislikesCountElement.text(json.dislikes);
            var date = new Date(1000 * json.date);
            var displayedDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
            this.imageDateElement.text(displayedDate);
            this.imageTranscriptElement.text(json.transcript);
            // Update URL with base56 encoded ID.
            //if(json.base56 != 0) {
            //    var newUrl = 'http://' + window.location.host + '/' + json.base56;
            //    window.history.replaceState({}, '', newUrl);
            //}
        });

        // Google Analytics.
        // Remember id of this image.
        var lastId = this.currentImage;
        setTimeout(() => {
            // If the user is still looking at the image after 1,6 seconds.
            if (lastId === this.currentImage && this.isOpened()) {
                // Send a page view to google analytics.
                ga('send', 'pageview', newUrl);
                // Send image view to our servers.
                var url = window.location.protocol + '//' + window.location.host +
                    '/api/image/' + this.currentImage + '/view';
                $.post(url);
            }
        }, 1600);
    }

    /**
     * Likes current image.
     */
    public like() {
        if (this.isOpened()) {
            // Check if logged in.
            if (false) {
                // TODO: Display login screen.
            }

            var newCount = parseInt(this.likesCountElement.text(), 10) + 1;
            this.likesCountElement.text(newCount.toString(10));

            // Easter egg: play sound effect.
            var el = $('<audio><source src="//coolovo.eu/dist/haha.mp3" type="audio/mpeg"></audio>');
            $(document.body).append(el);
            el.trigger('play');

            // Send AJAX request.
            // this.client.like(this.currentImage);
        }
    }

    /**
     * Dislikes current image.
     */
    public dislike() {
        if (this.isOpened()) {
            // Check if logged in.
            if (false) {
                // TODO: Display login screen.
            }

            var newCount = parseInt(this.dislikesCountElement.text(), 10) + 1;
            this.dislikesCountElement.text(newCount.toString(10));

            // Send AJAX request.
            // this.client.dislike(this.currentImage);
        }
    }

    /**
     * Opens facebook sharer for current image.
     */
    public facebookShare() {
        if (this.isOpened()) {
            // Open facebook share on other card.
            var shared_url = window.location.protocol + '//' + window.location.host + '/' + base56_encode(this.currentImage);
            var url = 'https://www.facebook.com/sharer/sharer.php?u=' + shared_url;
            window.open(url, '_blank');
        }
    }

    /**
     * Opens twitter sharer for current image.
     */
    public twitterShare() {
        if (this.isOpened()) {
            // Open twitter share on other card.
            var shared_url = window.location.protocol + '//' + window.location.host + '/' + base56_encode(this.currentImage);
            var url = 'https://twitter.com/intent/tweet?text=' + shared_url;
            window.open(url, '_blank');
        }
    }

    /**
     * Downloads current image.
     */
    public download() {
        if (this.isOpened()) {
            // Open facebook share on other card.
            var url = window.location.protocol + '//' + window.location.host + '/api/image/' + this.currentImage + '?download=download';
            window.open(url, '_self');
        }
    }

    public getComponentName():string {
        return 'Viewer';
    }
}