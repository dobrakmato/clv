/// <reference path="component.ts" />
/// <reference path="references.ts" />
/// <reference path="wrapper.ts" />


/**
 * Wraps access to "spinner" loader element.
 */
class Spinner extends Wrapper {

    constructor() {
        super('#spinner');
    }

    /**
     * Shows the spinner.
     */
    public show() {
        this.element.show();
    }

    /**
     * Hides the spinner.
     */
    public hide() {
        this.element.hide();
    }

    public getComponentName():string {
        return 'Spinnner';
    }
}