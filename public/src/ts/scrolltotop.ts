/// <reference path="component.ts" />
/// <reference path="grid.ts" />
/// <reference path="references.ts" />
/// <reference path="wrapper.ts" />


/**
 * Class that provides scroll to top component.
 */
class ScrollToTop extends Wrapper {

    private scrollableElement:Wrapper;
    private lastScrollTop:number;

    constructor() {
        super('#scrollToTop');

        this.scrollableElement = this.app.getComponent(Grid.prototype);

        this.registerScroll();
        this.registerClick();
    }

    public registerClick() {
        this.element.click(() => {
            // Scroll to top.
            this.scrollableElement.element.animate({scrollTop: 0}, {duration: '500', easing: 'swing'});
        });
    }

    public registerScroll() {
        this.scrollableElement.element.scroll(() => {
            var st = this.scrollableElement.element.scrollTop();
            if (st > 300 && st < this.lastScrollTop) {
                this.element.css('right', '0em');
            } else {
                this.element.css('right', '-15em');
            }
            this.lastScrollTop = st;
        });
    }

    public getComponentName():string {
        return 'ScrollToTop';
    }
}