var Component = (function () {
    function Component() {
        this.app = Component.app_ref;
    }
    Component.prototype.getComponentName = function () {
        throw new Error('Derived class does not override getComponentName() function!');
    };
    return Component;
})();
var Constants = (function () {
    function Constants() {
    }
    Constants.init = function () {
        if (Constants.IS_MOBILE) {
            Constants.GALLERY_SLIDE_IN_LENGTH = 500;
            Constants.VIEWER_CHANGE_LENGTH = 200;
            Constants.VIEWER_SHOW_LENGTH = 500;
        }
        else {
            Constants.GALLERY_SLIDE_IN_LENGTH = 500;
            Constants.VIEWER_CHANGE_LENGTH = 200;
            Constants.VIEWER_SHOW_LENGTH = 200;
        }
    };
    Constants.IS_MOBILE = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    return Constants;
})();
Constants.init();
/// <reference path="../../typings/google.analytics/ga.d.ts" />
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="../../typings/mousetrap/mousetrap.d.ts" />
/// <reference path="../../typings/velocity-animate/velocity-animate.d.ts" /> 
/// <reference path="constants.ts" />
/// <reference path="references.ts" />
var SettingBuilder = (function () {
    function SettingBuilder(name) {
        this.name = name;
    }
    SettingBuilder.prototype.withDefault = function (defaultValue) {
        this.default = defaultValue;
        return this;
    };
    SettingBuilder.prototype.withDescripton = function (description) {
        this.description = description;
        return this;
    };
    SettingBuilder.prototype.build = function () {
        return new Setting(this.name, localStorage.getItem('settings.' + this.name) !== null ?
            localStorage.getItem('settings.' + this.name) : this.default, this.description);
    };
    return SettingBuilder;
})();
var Setting = (function () {
    function Setting(name, value, description) {
        this.name = name;
        this.value = value;
        this.description = description;
        this.addToSettings();
    }
    Setting.prototype.addToSettings = function () {
        var _this = this;
        if (this.asBoolean()) {
            var label = $('<label for="setting-' + this.name + '">' + '<input type="checkbox" name="setting-'
                + this.name + '" checked="checked"></input>' + this.description + '</label>');
        }
        else {
            var label = $('<label for="setting-' + this.name + '">' + '<input type="checkbox" name="setting-'
                + this.name + '"></input>' + this.description + '</label>');
        }
        label.children().first().click(function () {
            _this.value = _this.value === 'true' ? 'false' : 'true';
            _this.save();
        });
        var div = $('<div/>');
        div.append(label);
        Setting.settingsPage.append(div);
    };
    Setting.prototype.getName = function () {
        return this.name;
    };
    Setting.prototype.getDescription = function () {
        return this.description;
    };
    Setting.prototype.getValue = function () {
        return this.value;
    };
    Setting.prototype.asBoolean = function () {
        return this.value === 'true';
    };
    Setting.prototype.asInteger = function () {
        return parseInt(this.value, 10);
    };
    Setting.prototype.asFloat = function () {
        return parseFloat(this.value);
    };
    Setting.prototype.save = function () {
        localStorage.setItem('settings.' + this.name, this.value);
    };
    Setting.of = function (name) {
        return new SettingBuilder(name);
    };
    Setting.settingsPage = $('#settings_holder');
    return Setting;
})();
var Settings = (function () {
    function Settings() {
    }
    Settings.USE_ANIMATIONS = Setting
        .of('animations')
        .withDescripton('Use nice animations in the page.')
        .withDefault('true')
        .build();
    Settings.CHECK_FOR_NEW_POSTS = Setting
        .of('autocheck')
        .withDescripton('Automatically check for new posts.')
        .withDefault(Constants.IS_MOBILE ? 'false' : 'true')
        .build();
    Settings.PLAY_SOUND_NEW_POSTS = Setting
        .of('autocheck-sound')
        .withDescripton('Play sound effect when there are new posts.')
        .withDefault('true')
        .build();
    Settings.USE_SHORT_URLS = Setting
        .of('base56urls')
        .withDescripton('Use short URLs where possible.')
        .withDefault('true')
        .build();
    return Settings;
})();
function bindSettingsButtons() {
    $('#settings-btn').click(function () {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            $('#overlay').velocity('transition.fadeIn', 300);
            $('#settings').velocity('transition.fadeIn', 300);
        }
        else {
            $('#overlay').css('display', 'block');
            $('#settings').css('display', 'block');
        }
    });
    $('#settings-close-btn').click(function () {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            $('#overlay').velocity('transition.fadeOut', 300);
            $('#settings').velocity('transition.fadeOut', 300);
        }
        else {
            $('#overlay').css('display', 'none');
            $('#settings').css('display', 'none');
        }
    });
}
bindSettingsButtons();
/// <reference path="component.ts" />
/// <reference path="references.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Wrapper = (function (_super) {
    __extends(Wrapper, _super);
    function Wrapper(selector) {
        _super.call(this);
        this.element = $(selector);
    }
    return Wrapper;
})(Component);
/// <reference path="component.ts" />
/// <reference path="references.ts" />
/// <reference path="wrapper.ts" />
var Spinner = (function (_super) {
    __extends(Spinner, _super);
    function Spinner() {
        _super.call(this, '#spinner');
    }
    Spinner.prototype.show = function () {
        this.element.show();
    };
    Spinner.prototype.hide = function () {
        this.element.hide();
    };
    Spinner.prototype.getComponentName = function () {
        return 'Spinnner';
    };
    return Spinner;
})(Wrapper);
var alphabet = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'.split('');
var tebahpla = alphabet.slice().reverse();
function base56_encode(num) {
    if (num === 0) {
        return '0';
    }
    var arr = [];
    while (num > 0) {
        var rem = num % 56;
        num = Math.floor(num / 56);
        arr.push(alphabet[rem]);
    }
    arr = arr.reverse();
    return arr.join('');
}
function base56_decode(str) {
    var num = 0;
    var idx = 0;
    for (var i = 0; i < str.length; i++) {
        var char = str.charAt(i);
        var power = (str.length - (idx + 1));
        num += tebahpla.indexOf(char) * (Math.pow(56, power));
        idx += 1;
    }
    return num;
}
var Preloader = (function (_super) {
    __extends(Preloader, _super);
    function Preloader() {
        _super.call(this);
        this.createMainNode();
    }
    Preloader.prototype.createMainNode = function () {
        this.preloadRoot = $('<div/>')
            .attr('id', 'preload-root')
            .css('display', 'none')
            .appendTo(document.body);
    };
    Preloader.prototype.preloadImage = function (url) {
        var element = $('<img/>')
            .one('load', function () {
            element.remove();
        })
            .attr('src', url)
            .appendTo(this.preloadRoot);
    };
    Preloader.prototype.getComponentName = function () {
        return 'Preloader';
    };
    return Preloader;
})(Component);
/// <reference path="b56.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="references.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="wrapper.ts" />
/// <reference path="preloader.ts" />
var ImageViewer = (function (_super) {
    __extends(ImageViewer, _super);
    function ImageViewer() {
        var _this = this;
        _super.call(this, '#viewer');
        this.grid = this.app.getComponent(Grid.prototype);
        this.client = this.app.getComponent(Client.prototype);
        this.preloader = this.app.getComponent(Preloader.prototype);
        this.imageElement = this.element.children('.viewer-image');
        this.titleElement = this.element.find('.viewer-title');
        this.likesCountElement = this.element.find('.like-count');
        this.dislikesCountElement = this.element.find('.dislike-count');
        this.imageDateElement = $('#image-date');
        this.imageTranscriptElement = $('#image-transcript');
        this.imageOriginalLink = $('#image-original');
        this.animationScore = 0;
        this.registerTouchEvents();
        this.element.find('#like-btn').click(function () {
            _this.like();
        });
        this.element.find('#dislike-btn').click(function () {
            _this.dislike();
        });
        this.element.find('#fshare-btn').click(function () {
            _this.facebookShare();
        });
        this.element.find('#twittershare-btn').click(function () {
            _this.twitterShare();
        });
        this.element.find('#download-btn').click(function () {
            _this.download();
        });
        this.element.find('#report-btn').click(function () {
            var msg = prompt('Aký je problém s týmto obrázkom?');
            if (msg !== '' && msg != null && msg.length > 3) {
                $.getJSON('/api/report?id=' + _this.currentImage + '&msg=' + msg, function (data) {
                    alert(data.status);
                });
            }
            else {
                alert('Ak chcete tento obrazok nahlasit, musite zadat spravu, ktora bude dlhsia ako 3 znaky.');
            }
        });
        this.element.find('#close-btn').click(function (e) {
            _this.close();
            e.stopImmediatePropagation();
        });
        this.imageElement.click(function (e) {
            var f = e.pageX / $(document.body).width();
            if (f > 0.5) {
                _this.next();
            }
            else {
                _this.prev();
            }
        });
    }
    ImageViewer.prototype.registerTouchEvents = function () {
        var _this = this;
        this.element.on('swipeLeft', function () {
            _this.next();
        });
        this.element.on('swipeRight', function () {
            _this.prev();
        });
        this.element.on('swipeDown', function () {
            if (_this.element.scrollTop() < 5) {
                _this.close();
            }
        });
        this.element.on('doubleTap', function () {
            _this.like();
        });
    };
    ImageViewer.prototype.isOpened = function () {
        return this.element.css('top') === '0px';
    };
    ImageViewer.prototype.close = function () {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.velocity({ 'top': '100%' }, Constants.VIEWER_SHOW_LENGTH);
        }
        else {
            this.element.css('top', '100%');
        }
        if (typeof sharedImage !== 'undefined') {
            var id = Settings.USE_SHORT_URLS.asBoolean() ? base56_encode(sharedImage) : sharedImage;
            window.history.pushState({}, '', window.location.protocol + '//' + window.location.host + '/' + id);
        }
        else {
            window.history.pushState({}, '', window.location.protocol + '//' + window.location.host + '/' + this.client.getEndpoint());
        }
    };
    ImageViewer.prototype.open = function () {
        if (Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.velocity({ 'top': '0%' }, Constants.VIEWER_SHOW_LENGTH);
        }
        else {
            this.element.css('top', '0%');
        }
    };
    ImageViewer.prototype.animateNext = function (newId) {
        var _this = this;
        this.animationScore++;
        this.imageElement.velocity({ 'opacity': '0.0' }, Constants.VIEWER_CHANGE_LENGTH, function () {
            _this.setImage(newId);
            _this.imageElement.velocity({ 'opacity': '1.0' }, Constants.VIEWER_CHANGE_LENGTH, function () {
                _this.animationScore = 0;
            });
        });
    };
    ImageViewer.prototype.animatePrev = function (newId) {
        var _this = this;
        this.animationScore++;
        this.imageElement.velocity({ 'opacity': '0.0' }, Constants.VIEWER_CHANGE_LENGTH, function () {
            _this.setImage(newId);
            _this.imageElement.velocity({ 'opacity': '1.0' }, Constants.VIEWER_CHANGE_LENGTH, function () {
                _this.animationScore = 0;
            });
        });
    };
    ImageViewer.prototype.next = function () {
        var _this = this;
        if (this.isOpened()) {
            if (this.grid.indexOf(this.currentImage) === this.grid.size() - 3) {
                this.client.more(function (json) {
                    _this.grid.append(json);
                });
            }
            var nextId = this.grid.getAfter(this.currentImage);
            if (this.currentImage !== nextId) {
                if (this.animationScore < 1 && Settings.USE_ANIMATIONS.asBoolean()) {
                    this.animateNext(nextId);
                }
                else {
                    this.setImage(nextId);
                }
            }
        }
    };
    ImageViewer.prototype.prev = function () {
        if (this.isOpened()) {
            var prevId = this.grid.getBefore(this.currentImage);
            if (this.currentImage !== prevId) {
                if (this.animationScore < 1 && Settings.USE_ANIMATIONS.asBoolean()) {
                    this.animatePrev(prevId);
                }
                else {
                    this.setImage(prevId);
                }
            }
        }
    };
    ImageViewer.prototype.setImage = function (id) {
        var _this = this;
        this.currentImage = id;
        this.imageElement.css('background-image', 'url(' + this.client.getImageURL(id) + ')');
        this.titleElement.text(id);
        this.likesCountElement.text('0');
        this.dislikesCountElement.text('0');
        if (Settings.USE_SHORT_URLS.asBoolean()) {
            var newUrl = window.location.protocol + '//' + window.location.host + '/' + base56_encode(id);
            window.history.pushState({}, '', newUrl);
        }
        else {
            var newUrl = window.location.protocol + '//' + window.location.host + '/' + id;
            window.history.pushState({}, '', newUrl);
        }
        this.preloader.preloadImage(this.client.getImageURL(this.grid.getAfter(id)));
        var originalUrl = 'https://facebook.com/' + id;
        this.imageOriginalLink.text(originalUrl).attr('href', originalUrl);
        this.client.details(this.currentImage, function (json) {
            _this.likesCountElement.text(json.likes);
            _this.dislikesCountElement.text(json.dislikes);
            var date = new Date(1000 * json.date);
            var displayedDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
            _this.imageDateElement.text(displayedDate);
            _this.imageTranscriptElement.text(json.transcript);
        });
        var lastId = this.currentImage;
        setTimeout(function () {
            if (lastId === _this.currentImage && _this.isOpened()) {
                ga('send', 'pageview', newUrl);
                var url = window.location.protocol + '//' + window.location.host +
                    '/api/image/' + _this.currentImage + '/view';
                $.post(url);
            }
        }, 1600);
    };
    ImageViewer.prototype.like = function () {
        if (this.isOpened()) {
            if (false) {
            }
            var newCount = parseInt(this.likesCountElement.text(), 10) + 1;
            this.likesCountElement.text(newCount.toString(10));
            var el = $('<audio><source src="//coolovo.eu/dist/haha.mp3" type="audio/mpeg"></audio>');
            $(document.body).append(el);
            el.trigger('play');
        }
    };
    ImageViewer.prototype.dislike = function () {
        if (this.isOpened()) {
            if (false) {
            }
            var newCount = parseInt(this.dislikesCountElement.text(), 10) + 1;
            this.dislikesCountElement.text(newCount.toString(10));
        }
    };
    ImageViewer.prototype.facebookShare = function () {
        if (this.isOpened()) {
            var shared_url = window.location.protocol + '//' + window.location.host + '/' + base56_encode(this.currentImage);
            var url = 'https://www.facebook.com/sharer/sharer.php?u=' + shared_url;
            window.open(url, '_blank');
        }
    };
    ImageViewer.prototype.twitterShare = function () {
        if (this.isOpened()) {
            var shared_url = window.location.protocol + '//' + window.location.host + '/' + base56_encode(this.currentImage);
            var url = 'https://twitter.com/intent/tweet?text=' + shared_url;
            window.open(url, '_blank');
        }
    };
    ImageViewer.prototype.download = function () {
        if (this.isOpened()) {
            var url = window.location.protocol + '//' + window.location.host + '/api/image/' + this.currentImage + '?download=download';
            window.open(url, '_self');
        }
    };
    ImageViewer.prototype.getComponentName = function () {
        return 'Viewer';
    };
    return ImageViewer;
})(Wrapper);
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="references.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />
/// <reference path="preloader.ts" />
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid() {
        _super.call(this, '#gallery');
        this.scrollableElement = $('#scrollable-element');
        this.client = this.app.getComponent(Client.prototype);
        this.preloader = this.app.getComponent(Preloader.prototype);
        this.registerTouchEvents();
        this.preloadFirst();
    }
    Grid.prototype.preloadFirst = function () {
        this.preloader.preloadImage(this.client.getImageURL(this.firstId()));
    };
    Grid.prototype.registerTouchEvents = function () {
        var _this = this;
        this.scrollableElement.on('swipeDown', function () {
            if (_this.scrollableElement.scrollTop() < 10) {
                _this.clear(function () {
                    $('#spinner').show();
                    window.location = window.location;
                });
            }
        });
    };
    Grid.prototype.show = function () {
        if (!Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.css('display', 'block');
            return;
        }
        if (Constants.IS_MOBILE) {
            this.element.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
        }
        else {
            this.element.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
        }
    };
    Grid.prototype.clear = function (callback) {
        var _this = this;
        if (!Settings.USE_ANIMATIONS.asBoolean()) {
            this.element.empty();
            if (callback) {
                callback();
            }
            return;
        }
        var animation = 'transition.slideDownOut';
        if (Constants.IS_MOBILE) {
            animation = 'transition.fadeOut';
        }
        this.element.velocity(animation, Constants.GALLERY_SLIDE_IN_LENGTH, function () {
            _this.element.empty();
            if (callback) {
                callback();
            }
        });
    };
    Grid.prototype.size = function () {
        return this.element.children().length;
    };
    Grid.prototype.indexOf = function (id) {
        return $('#' + id).index();
    };
    Grid.prototype.firstId = function () {
        return this.getImageIdFromElement(this.element.children().first());
    };
    Grid.prototype.getAfter = function (id) {
        if (id === undefined) {
            console.error('Current image is undefined!');
            return this.getImageIdFromElement(this.element.children().first());
        }
        var index = $('#' + id).index();
        if (this.size() > index + 1) {
            return this.getImageIdFromElement(this.element.children().eq(index + 1));
        }
        else {
            console.error('Current index is: ' + index + ', size: ' + this.size() + ', but user requested more!');
            return id;
        }
    };
    Grid.prototype.getBefore = function (id) {
        if (id === undefined) {
            console.error('Current image is undefined!');
            return this.getImageIdFromElement(this.element.children().first());
        }
        var index = $('#' + id).index();
        if (0 <= index - 1) {
            return this.getImageIdFromElement(this.element.children().eq(index - 1));
        }
        else {
            console.error('Current index is: ' + index + ', size: ' + this.size() + ', but user requested less!');
            return this.getImageIdFromElement(this.element.children().first());
        }
    };
    Grid.prototype.getImageIdFromElement = function (element) {
        return element.attr(Grid.IMAGE_ID_ATTR);
    };
    Grid.prototype.append = function (json) {
        for (var key in json) {
            if (json.hasOwnProperty(key)) {
                var item = json[key];
                var image_url = this.client.getImageThumbnailURL(item.id);
                var innerElement = $('<a/>')
                    .attr('href', '//' + window.location.hostname + '/' + item.id)
                    .addClass('thumb')
                    .css('background-image', 'url(' + image_url + ')');
                var outerElement = $('<div/>')
                    .addClass('outer ns')
                    .attr(Grid.IMAGE_ID_ATTR, item.id);
                this.element.append(outerElement.append(innerElement));
            }
        }
    };
    Grid.prototype.prependElement = function (el) {
        this.element.prepend(el);
    };
    Grid.prototype.getComponentName = function () {
        return 'Grid';
    };
    Grid.IMAGE_ID_ATTR = 'id';
    return Grid;
})(Wrapper);
/// <reference path='component.ts' />
/// <reference path='grid.ts' />
/// <reference path='references.ts' />
var InfiniteScroll = (function (_super) {
    __extends(InfiniteScroll, _super);
    function InfiniteScroll(preloadCallback) {
        var _this = this;
        _super.call(this);
        this.scrollableElement = $('#scrollable-content');
        this.fired = false;
        if (preloadCallback) {
            this.cb = preloadCallback;
        }
        this.scrollableElement.scroll(function () {
            if (_this.scrollableElement.scrollTop() > _this.scrollableElement.prop('scrollHeight') -
                _this.scrollableElement.height() - 300 && _this.fired === false) {
                _this.fired = true;
                _this.cb();
            }
        });
    }
    InfiniteScroll.prototype.setCallback = function (callback) {
        this.cb = callback;
    };
    InfiniteScroll.prototype.reset = function () {
        this.fired = false;
    };
    InfiniteScroll.prototype.getComponentName = function () {
        return 'InfiniteScroll';
    };
    return InfiniteScroll;
})(Component);
/// <reference path="references.ts" />
var ButtonCollection = (function () {
    function ButtonCollection(msgbox) {
        this.msgbox_parent = msgbox;
    }
    ButtonCollection.prototype.button = function (text, callback, className) {
        if (className === void 0) { className = 'btn-normal'; }
        var button = $('<div/>')
            .addClass('btn ' + className)
            .text(text)
            .click(callback);
        this.msgbox_parent.addToFooter(button);
        return this;
    };
    ButtonCollection.prototype.parent = function () {
        return this.msgbox_parent;
    };
    return ButtonCollection;
})();
var MessageBox = (function () {
    function MessageBox(title, content) {
        this._buttons = new ButtonCollection(this);
        this.createDOM(title, content);
        this.initializeOverlay();
    }
    MessageBox.prototype.show = function (velocity, lenght) {
        if (lenght === void 0) { lenght = 300; }
        MessageBox.overlayElement.velocity('fadeIn', lenght);
        this.element.velocity(velocity, lenght);
    };
    MessageBox.prototype.hide = function (velocity, lenght) {
        if (lenght === void 0) { lenght = 300; }
        this.element.velocity(velocity, lenght);
        MessageBox.overlayElement.velocity('fadeOut', lenght);
    };
    MessageBox.prototype.buttons = function () {
        return this._buttons;
    };
    MessageBox.prototype.addToFooter = function (element) {
        this.buttonsElement.append(element);
    };
    MessageBox.prototype.createDOM = function (title, content) {
        this.titleElement = $('<div/>')
            .addClass('msgbox-title')
            .text(title);
        this.contentElement = $('<div/>')
            .addClass('msgbox-content')
            .html(content);
        this.buttonsElement = $('<div/>')
            .addClass('msgbox-buttons');
        this.element = $('<div/>')
            .addClass('msgbox')
            .append(this.titleElement)
            .append(this.contentElement)
            .append(this.buttonsElement);
    };
    MessageBox.prototype.initializeOverlay = function () {
        if (MessageBox.overlayElement != null) {
            return;
        }
        var overlay = $('#overlay');
        if (overlay.length === 0) {
            overlay = $('.overlay');
        }
        if (overlay.length === 0) {
            overlay = $('<div/>')
                .attr('id', 'overlay')
                .addClass('overlay');
            MessageBox.overlayElement = overlay;
        }
    };
    return MessageBox;
})();
/// <reference path="component.ts" />
/// <reference path="grid.ts" />
/// <reference path="references.ts" />
/// <reference path="wrapper.ts" />
var ScrollToTop = (function (_super) {
    __extends(ScrollToTop, _super);
    function ScrollToTop() {
        _super.call(this, '#scrollToTop');
        this.scrollableElement = this.app.getComponent(Grid.prototype);
        this.registerScroll();
        this.registerClick();
    }
    ScrollToTop.prototype.registerClick = function () {
        var _this = this;
        this.element.click(function () {
            _this.scrollableElement.element.animate({ scrollTop: 0 }, { duration: '500', easing: 'swing' });
        });
    };
    ScrollToTop.prototype.registerScroll = function () {
        var _this = this;
        this.scrollableElement.element.scroll(function () {
            var st = _this.scrollableElement.element.scrollTop();
            if (st > 300 && st < _this.lastScrollTop) {
                _this.element.css('right', '0em');
            }
            else {
                _this.element.css('right', '-15em');
            }
            _this.lastScrollTop = st;
        });
    };
    ScrollToTop.prototype.getComponentName = function () {
        return 'ScrollToTop';
    };
    return ScrollToTop;
})(Wrapper);
/// <reference path="application.ts" />
/// <reference path="bootstrap.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />
var Map = (function () {
    function Map() {
        this.obj = {
            'dont_be_sad': 'coolovo.eu'
        };
    }
    Map.prototype.containsKey = function (key) {
        return this.obj.hasOwnProperty(key);
    };
    Map.prototype.get = function (key) {
        if (this.containsKey(key)) {
            return this.obj[key];
        }
        else {
            return null;
        }
    };
    Map.prototype.put = function (key, value) {
        if (key == null) {
            throw new Error('Key is null!');
        }
        this.obj[key] = value;
    };
    Map.prototype.remove = function (key) {
        delete this.obj[key];
    };
    return Map;
})();
/// <reference path="application.ts" />
/// <reference path="bootstrap.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="map.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />
var Client = (function (_super) {
    __extends(Client, _super);
    function Client() {
        var _this = this;
        _super.call(this);
        this.start = Client.LOAD_COUNT;
        this.end = Client.LOAD_COUNT * 2;
        setTimeout(function () {
            $.getJSON('/api/token', function (json) {
                if (json.token) {
                    _this.token = json.token;
                }
            });
        }, 1000);
    }
    Client.prototype.setEndpoint = function (endpoint, startFromZero) {
        if (startFromZero === void 0) { startFromZero = false; }
        this.endpoint = endpoint;
        if (startFromZero) {
            this.start = 0;
            this.end = Client.LOAD_COUNT;
        }
        else {
            this.start = Client.LOAD_COUNT;
            this.end = Client.LOAD_COUNT * 2;
        }
    };
    Client.prototype.getEndpointURL = function () {
        return this.endpoint;
    };
    Client.prototype.getEndpoint = function () {
        if (this.endpoint.indexOf('random') > -1) {
            return 'random/' + this.endpoint.substr(this.endpoint.lastIndexOf('/') + 1);
        }
        else {
            return this.endpoint.substr(this.endpoint.lastIndexOf('/') + 1);
        }
    };
    Client.prototype.more = function (callback) {
        var _this = this;
        $.getJSON(this.getURL(), function (data) {
            var count = _this.end - _this.start;
            _this.start = _this.end;
            _this.end += count;
            callback(data);
        });
    };
    Client.prototype.details = function (id, callback) {
        $.getJSON(this.getDetailsURL(id), function (data) {
            callback(data);
        });
    };
    Client.prototype.getOneLatest = function (callback) {
        $.getJSON(this.endpoint + '?start=0&end=1', function (data) {
            callback(data);
        });
    };
    Client.prototype.getDetailsURL = function (id) {
        return '/api/image/' + id + '/details';
    };
    Client.prototype.getURL = function () {
        if (this.endpoint.indexOf('?') > -1) {
            return this.endpoint + '&start=' + this.start + '&end=' + this.end;
        }
        else {
            return this.endpoint + '?start=' + this.start + '&end=' + this.end;
        }
    };
    Client.prototype.getImageThumbnailURL = function (id) {
        return '/api/image/' + id + '?size=thumbnail&token=' + this.token;
    };
    Client.prototype.getImageURL = function (id) {
        return '/api/image/' + id + '?size=original&token=' + this.token;
    };
    Client.prototype.getComponentName = function () {
        return 'Client';
    };
    Client.LOAD_COUNT = 30;
    return Client;
})(Component);
/// <reference path="references.ts" />
/// <reference path="component.ts" />
var SoundEffect = (function (_super) {
    __extends(SoundEffect, _super);
    function SoundEffect() {
        _super.call(this);
        this.audioElement = null;
    }
    SoundEffect.prototype.play = function () {
        if (this.audioElement === null) {
            this.createAudioElement();
        }
        this.audioElement.trigger('play');
    };
    SoundEffect.prototype.getComponentName = function () {
        return 'SoundEffect';
    };
    SoundEffect.prototype.createAudioElement = function () {
        var el = $('<audio><source src="' + SoundEffect.URL + '" type="audio/mpeg" preload="auto"></audio>')
            .css('display', 'none');
        el.appendTo(document.body);
        this.audioElement = el;
    };
    SoundEffect.URL = '//coolovo.eu/dist/notification.mp3';
    return SoundEffect;
})(Component);
/// <reference path="application.ts" />
/// <reference path="client.ts" />
/// <reference path="component.ts" />
/// <reference path="constants.ts" />
/// <reference path="grid.ts" />
/// <reference path="infinitescroll.ts" />
/// <reference path="map.ts" />
/// <reference path="messagebox.ts" />
/// <reference path="preloader.ts" />
/// <reference path="references.ts" />
/// <reference path="scrolltotop.ts" />
/// <reference path="settings.ts" />
/// <reference path="soundeffect.ts" />
/// <reference path="spinner.ts" />
/// <reference path="viewer.ts" />
/// <reference path="wrapper.ts" />
var Bootstrap = (function () {
    function Bootstrap() {
        var _this = this;
        this.app = new Application();
        Component.app_ref = this.app;
        this.app.addComponent(new Preloader());
        this.app.addComponent(new Client());
        this.app.addComponent(new Spinner());
        this.app.addComponent(new Grid());
        this.app.addComponent(new ImageViewer());
        this.app.addComponent(new ScrollToTop());
        this.app.addComponent(new InfiniteScroll());
        this.app.addComponent(new SoundEffect());
        this.installInfiniteScroll();
        this.ajaxify();
        Bootstrap.installMobileFixes();
        if (dontBoot === true) {
            this.app.getComponent(Spinner.prototype).hide();
            this.app.getComponent(Grid.prototype).show();
            var el = $('#scrollable-content');
            if (!Settings.USE_ANIMATIONS.asBoolean()) {
                el.css('display', 'block');
                this.bindThumbClick();
                return;
            }
            if (Constants.IS_MOBILE) {
                el.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }
            else {
                el.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }
            return;
        }
        this.bindKeyboard();
        this.installNewPostsChecker();
        this.installBackButton();
        if (typeof streamPath === 'undefined') {
            console.error('Stream endpoint not provided!');
            this.app.getComponent(Client.prototype).setEndpoint('api/stream/latest');
        }
        else {
            this.app.getComponent(Client.prototype).setEndpoint(streamPath);
        }
        setTimeout(function () {
            _this.app.getComponent(Spinner.prototype).hide();
            _this.app.getComponent(Grid.prototype).show();
            var el = $('#scrollable-content');
            if (!Settings.USE_ANIMATIONS.asBoolean()) {
                el.css('display', 'block');
                _this.bindThumbClick();
                return;
            }
            if (Constants.IS_MOBILE) {
                el.velocity('transition.fadeIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }
            else {
                el.velocity('transition.slideUpIn', Constants.GALLERY_SLIDE_IN_LENGTH);
            }
            if (!Constants.IS_MOBILE) {
                $(document.body).append($('<style>#viewer{background:rgba(0,0,0,0.92);}</style>'));
            }
            _this.bindThumbClick();
        }, 300);
    }
    Bootstrap.prototype.installNewPostsChecker = function () {
        var _this = this;
        if (Settings.CHECK_FOR_NEW_POSTS.asBoolean()) {
            var task_id = setInterval(function () {
                _this.app.getComponent(Client.prototype).getOneLatest(function (json) {
                    var localFirstId = _this.app.getComponent(Grid.prototype).firstId();
                    if (localFirstId !== json[0].id) {
                        var message = $('<div class="centered"><div class="newposts">'
                            + 'New posts! Refresh the page to see new images!</div></div>');
                        message.click(function () {
                            window.location = window.location;
                        });
                        _this.app.getComponent(Grid.prototype).prependElement(message);
                        window.document.title = '[NEW!] ' + window.document.title;
                        if (Settings.PLAY_SOUND_NEW_POSTS.asBoolean()) {
                            _this.app.getComponent(SoundEffect.prototype).play();
                        }
                        clearInterval(task_id);
                    }
                });
            }, 30000);
        }
    };
    Bootstrap.prototype.installInfiniteScroll = function () {
        var _this = this;
        var infiniteScroll = this.app.getComponent(InfiniteScroll.prototype);
        infiniteScroll.setCallback(function () {
            _this.app.getComponent(Client.prototype).more(function (json) {
                if (!jQuery.isEmptyObject(json)) {
                    _this.app.getComponent(Grid.prototype).append(json);
                    _this.bindThumbClick();
                    infiniteScroll.reset();
                }
            });
        });
    };
    Bootstrap.installMobileFixes = function () {
        if (Constants.IS_MOBILE) {
            $(document.body).append($('<style>::-webkit-scrollbar{display:none;}' +
                '.thumb{background-size:contain; background-repeat:no-repeat;}</style>'));
            $(document.body).append($('<style>.thumb{display:block;background-size:contain;' +
                'background-repeat:no-repeat;height:25em;width:initial;}.outer{padding:0;display:block;}</style>'));
        }
    };
    Bootstrap.prototype.bindThumbClick = function () {
        var self = this;
        $('.outer').click(function (e) {
            e.preventDefault();
            self.app.getComponent(ImageViewer.prototype).setImage($(this).attr('id'));
            self.app.getComponent(ImageViewer.prototype).open();
        });
    };
    Bootstrap.prototype.ajaxify = function () {
        var _this = this;
        $('#navbar a').click(function () {
            _this.app.getComponent(Grid.prototype).element.hide();
            _this.app.getComponent(Spinner.prototype).show();
        });
    };
    Bootstrap.prototype.bindKeyboard = function () {
        var _this = this;
        Mousetrap.bind(['o'], function () {
            _this.app.getComponent(ImageViewer.prototype).open();
        });
        Mousetrap.bind(['esc', 'q'], function () {
            _this.app.getComponent(ImageViewer.prototype).close();
        });
        Mousetrap.bind(['left', 'a'], function () {
            _this.app.getComponent(ImageViewer.prototype).prev();
        });
        Mousetrap.bind(['right', 'd', 'space'], function () {
            _this.app.getComponent(ImageViewer.prototype).next();
        });
        Mousetrap.bind(['l'], function () {
            _this.app.getComponent(ImageViewer.prototype).like();
        });
        Mousetrap.bind('ctrl+y', function () {
            $(document.body).html('<iframe src="https://www.youtube.com/embed/6Z4vTWS0jHs" frameborder="0" ' +
                'style="height: 100%;width: 100%;" allowfullscreen></iframe>');
        });
        Mousetrap.bind(['s e a r c h', 'f3', 'ctrl+f'], function () {
            var terms = prompt('Search terms:');
            _this.app.getComponent(Client.prototype).setEndpoint('api/stream/search?q=' + terms, true);
            _this.app.getComponent(Grid.prototype).clear(function () {
                _this.app.getComponent(Client.prototype).more(function (json) {
                    _this.app.getComponent(Grid.prototype).append(json);
                    _this.app.getComponent(Grid.prototype).show();
                    _this.bindThumbClick();
                });
            });
            return false;
        });
    };
    Bootstrap.prototype.installBackButton = function () {
        var _this = this;
        window.onpopstate = function () {
            console.log('onNavigate');
            console.log('navigating back...');
            if (_this.app.getComponent(ImageViewer.prototype).isOpened()) {
                console.log('closing opened image viewer');
                _this.app.getComponent(ImageViewer.prototype).close();
            }
        };
    };
    return Bootstrap;
})();
/// <reference path="bootstrap.ts" />
/// <reference path="component.ts" />
/// <reference path="map.ts" />
var Application = (function () {
    function Application() {
        this.components = new Map();
        Component.app_ref = this;
    }
    Application.prototype.getComponent = function (type) {
        var name = type.getComponentName();
        if (this.components.containsKey(name)) {
            return this.components.get(name);
        }
        else {
            throw new Error('This application has no component like "' + name + '" !');
        }
    };
    Application.prototype.addComponent = function (component) {
        this.components.put(component.getComponentName(), component);
    };
    return Application;
})();
new Bootstrap;
var Attribute = (function () {
    function Attribute() {
    }
    return Attribute;
})();
/// <reference path="attribute.ts" />
/// <reference path="renderable.ts" />
var Const = (function () {
    function Const() {
    }
    Const.O = '<';
    Const.C = '>';
    Const.S = ' ';
    Const.SL = '/';
    Const.A = '"';
    Const.E = '=';
    return Const;
})();
var WrappedElement = (function () {
    function WrappedElement(ref) {
        this.ref = ref;
    }
    WrappedElement.prototype.append = function (element) {
        this.ref.innerHTML = this.ref.innerHTML + element.html();
    };
    WrappedElement.prototype.prepend = function (element) {
        this.ref.innerHTML = element.html() + this.ref.innerHTML;
    };
    WrappedElement.prototype.appendTo = function (element) {
        element.append(this);
    };
    WrappedElement.prototype.prependTo = function (element) {
        element.prepend(this);
    };
    WrappedElement.prototype.html = function (html) {
        if (html) {
            this.ref.innerHTML = html;
        }
        else {
            return this.ref.innerHTML;
        }
    };
    WrappedElement.prototype.attr = function (name, value) {
        if (value) {
            this.ref.setAttribute(name, value);
        }
        else {
            return this.ref.getAttribute(name);
        }
    };
    return WrappedElement;
})();
var GeneratedElement = (function () {
    function GeneratedElement(type) {
        this.type = type;
    }
    GeneratedElement.prototype.append = function (element) {
        this.value = this.value + element.html();
    };
    GeneratedElement.prototype.prepend = function (element) {
        this.value = element.html() + this.value;
    };
    GeneratedElement.prototype.appendTo = function (element) {
        element.append(this);
    };
    GeneratedElement.prototype.prependTo = function (element) {
        element.prepend(this);
    };
    GeneratedElement.prototype.html = function (html) {
        if (html) {
            this.value = html;
            return this;
        }
        else {
            return html;
        }
    };
    GeneratedElement.prototype.attr = function (name, value) {
        if (value) {
            for (var i = 0; i < this.attributes.length; i++) {
                if (this.attributes[i].name === name) {
                    this.attributes[i].value = value.toString();
                    return this;
                }
            }
        }
        else {
            for (var i = 0; i < this.attributes.length; i++) {
                if (this.attributes[i].name === name) {
                    return this.attributes[i].value;
                }
            }
        }
    };
    GeneratedElement.prototype.render = function () {
        var attributes = '';
        for (var i = 0; i < this.attributes.length; i++) {
            attributes += Const.S + this.attributes[i].name + Const.E + Const.A + this.attributes[i].value + Const.A;
        }
        var html = Const.O + this.type + attributes + Const.C + this.html +
            Const.O + Const.SL + this.type + Const.C;
        return html;
    };
    return GeneratedElement;
})();
function create(type) {
    return new GeneratedElement(type);
}
function select(id) {
    return new WrappedElement(document.getElementById(id));
}
/// <reference path="element.ts" />
var A = (function () {
    function A() {
    }
    A.prototype.a = function () {
        var gallery = select('gallery');
        for (var i = 0; i < 10; i++) {
            create('div')
                .attr('id', i)
                .attr('class', 'thumbnail')
                .appendTo(gallery);
        }
    };
    return A;
})();

//# sourceMappingURL=app.js.map