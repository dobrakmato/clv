// Function to disable "pull-to-refresh" effect present in some webviews.
// Especially Crosswalk 12 and above (Chromium 41+) runtimes.

// Adapted from this example: https://code.google.com/p/chromium/issues/detail?id=456515#c8
// Source: Copyright (c) 2015 by jdduke (http://jsbin.com/qofuwa/2/edit)
// Source: Released under the MIT license: http://jsbin.mit-license.org

// <input id="preventPullToRefresh"  type="checkbox">Prevent pull-to-refresh?</input>
// <input id="preventOverscrollGlow" type="checkbox">Prevent overscroll glow?</input>
// <input id="preventScroll"         type="checkbox">Prevent scroll?</input>

window.addEventListener('load', function() {
//  var preventPullToRefreshCheckbox  = document.getElementById('preventPullToRefresh') ;
//  var preventOverscrollGlowCheckbox = document.getElementById("preventOverscrollGlow") ;
//  var preventScrollCheckbox         = document.getElementById("preventScroll") ;

    var lastTouchY = 0 ;
    var maybePreventPullToRefresh = false ;

    // Pull-to-refresh will only trigger if the scroll begins when the
    // document's Y offset is zero.

    var touchstartHandler = function(e) {
        if( e.touches.length != 1 ) {
            return ;
        }
        lastTouchY = e.touches[0].clientY ;
        // maybePreventPullToRefresh = (preventPullToRefreshCheckbox.checked) && (window.pageYOffset == 0) ;
        maybePreventPullToRefresh = (window.pageYOffset == 0) ;
    }

    // To suppress pull-to-refresh it is sufficient to preventDefault the
    // first overscrolling touchmove.

    var touchmoveHandler = function(e) {
        var touchY = e.touches[0].clientY ;
        var touchYDelta = touchY - lastTouchY ;
        lastTouchY = touchY ;

        if (maybePreventPullToRefresh) {
            maybePreventPullToRefresh = false ;
            if (touchYDelta > 0) {
                e.preventDefault() ;
                // console.log("pull-to-refresh event detected") ;
                return ;
            }
        }

        // if (preventScrollCheckbox.checked) {
        //     e.preventDefault() ;
        //     return ;
        // }

        // if (preventOverscrollGlowCheckbox.checked) {
        //     if (window.pageYOffset == 0 && touchYDelta > 0) {
        //         e.preventDefault() ;
        //         return ;
        //     }
        // }
    }

    document.addEventListener('touchstart', touchstartHandler, false) ;
    document.addEventListener('touchmove', touchmoveHandler, false) ;
}) ;