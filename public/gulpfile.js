'use strict';

// Include gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var tslint = require('gulp-tslint');
var ts = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var imagemin; // Lazy load.

var Config = require('./gulpfile.config');
var config = new Config();

// SASS TASKS
gulp.task('sass', ['sass:compile']);

gulp.task('sass:compile', function () {
    gulp.src(config.sourceSass)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.distCss));
});

gulp.task('sass:watch', function () {
    gulp.watch(config.sourceSass, ['sass']);
});

// TS TASKS
gulp.task('ts', ['ts:lint', 'ts:compile']);

gulp.task('ts:lint', function () {
    gulp.src(config.sourceTypescript)
        .pipe(tslint())
        .pipe(tslint.report('verbose'));
});

gulp.task('ts:compile', function () {
    gulp.src(config.sourceTypescript)
        .pipe(sourcemaps.init())
        .pipe(ts({removeComments: true, target: 'ES3', out: 'app.js'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.sourceJavascriptFolder));
});

gulp.task('ts:watch', function () {
    gulp.watch(config.sourceTypescript, ['ts']);
});

// JS TASKS
gulp.task('js', ['js:minify']);

gulp.task('js:minify', function () {
    gulp.src(config.sourceJavascript)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify({mangle: true, compress: true}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.distJavascript));
});

gulp.task('js:watch', function () {
    gulp.watch(config.sourceTypescript, ['js']);
});

// IMG TASKS
gulp.task('img', ['img:optmize']);

gulp.task('img:optmize', function () {
    if(typeof imagemin === "undefined") {
        imagemin = require('gulp-imagemin');
    }

    return gulp.src(config.sourceImages)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(config.distImages));
});

gulp.task('img:watch', function () {
    if(typeof imagemin === "undefined") {
        imagemin = require('gulp-imagemin');
    }

    gulp.watch(config.sourceTypescript, ['img']);
});

// DEFAULT TASKS
gulp.task('default', ['sass', 'ts', 'js']);
gulp.task('watch', ['sass:watch', 'ts:watch', 'js:watch']);